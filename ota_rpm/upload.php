<?php
/*
  

  Copyright 2013 Eggcyte Inc All Rights Reserved.

 
*/

date_default_timezone_set('UTC');

require_once("./env_settings.php");
$date = new DateTime();
$filePrefix = $date->format('Y_m_d') . '_' . $date->format('H_i_s') . '_';

$cntFiles = 0; 
$sizeBytes = 0;

if (file_exists(MANIFEST_FILE))
   parseCSV(MANIFEST_FILE);

echo "\nnumber of files =".$numFiles."\n";
echo "package size =".$sizePackage."\n";
echo "software version =".$SWVersion."\n";
echo "hybrid mode=".$hybridMode."\n";
echo "===========Input Files================"."\n";
print_r($sourceFilesArray);
echo "======================================"."\n";

$ini_array = parse_ini_file("./ota_rpm.ini", true);

$iniPath = $ini_array['general_section']['manifest_ini_file_path'];
array_map('unlink', glob($iniPath."/*"));
$iniFname = $ini_array['general_section']['manifest_ini_file_path'] . $filePrefix . $ini_array['general_section']['manifest_ini_file'];
$enc_iniFname = $ini_array['general_section']['enc_manifest_ini_file_path'] . $filePrefix . $ini_array['general_section']['enc_manifest_ini_file'];

$pemkey = $ini_array['general_section']['pemkey'];

//encoding
for ($i=0; $i<$numFiles; $i++)
{
      $infile = $ini_array['general_section']['file_repo'].$sourceFilesArray[$i];
      $outfile = $ini_array['general_section']['file_repo'].$filePrefix.$sourceFilesArray[$i].'.enc';

      exec('openssl smime -encrypt -binary -aes-256-cbc -in '.$infile.' -out '.$outfile.' -outform DER '.$pemkey);

      if ($i==0) $encFileArray = array($outfile);
      else array_push($encFileArray, $outfile);
} 
//print_r($encFileArray);

//process OTA scripts
//------------------------------------------------------------------------------
processOTAScripts();
//------------------------------------------------------------------------------

//process patches 
//------------------------------------------------------------------------------
processPatches();
//------------------------------------------------------------------------------

file_put_contents($iniFname, "\n", FILE_APPEND | LOCK_EX);
$string = "[SOURCE_RPM_FILE_SECTION]"."\n";
file_put_contents($iniFname, $string, FILE_APPEND | LOCK_EX);
for ($i=0; $i<$numFiles; $i++)
{
      $md5string= exec('md5sum '.$ini_array['general_section']['file_repo'].$sourceFilesArray[$i].'| cut -d" " -f1,3 | tr \' \' \',\'');
      $pieces_1= explode(",", $md5string);
      $numberPieces_1 = count($pieces_1);
      if ($numberPieces_1 > 2) {
          echo "error, number of pieces when processing md5string of $sourceFilesArray[$i] > 2";
          die();
      } else {
        $md5val= $pieces_1[0];
        $filePath= $pieces_1[1];
        $pieces_2= explode("/", $filePath);
        $numberPieces_2 = count($pieces_2);
        $fname = $pieces_2[$numberPieces_2-1];
      }
      $Data = "fileName=".$filePrefix.$fname.'&'."md5=".$md5val;
      $string= "sourceFiles[] = "."\"".$Data."\""."\n";
      file_put_contents($iniFname, $string, FILE_APPEND | LOCK_EX);
}
      
file_put_contents($iniFname, "\n", FILE_APPEND | LOCK_EX);
$string = "[ENCODED_RPM_FILE_SECTION]"."\n"; 
file_put_contents($iniFname, $string, FILE_APPEND | LOCK_EX);
for ($i=0; $i<$numFiles; $i++)
{
      $md5string= exec('md5sum '.$encFileArray[$i].'| cut -d" " -f1,3 | tr \' \' \',\'');
      $pieces_1= explode(",", $md5string);
      $numberPieces_1 = count($pieces_1);
      if ($numberPieces_1 > 2) {
          echo "error, number of pieces when processing md5string of $encFileArray[$i]  > 2";
          die();
      } else {
        $md5val= $pieces_1[0];
        $filePath= $pieces_1[1];
        $pieces_2= explode("/", $filePath);
        $numberPieces_2 = count($pieces_2);
        $fname = $pieces_2[$numberPieces_2-1];
      }
 
      $Data = "serverURL=".$ini_array['server_section']['server_url_path'].'&'."fileName=".$fname.'&'."md5=".$md5val;
      $string= "encodedFiles[] = "."\"".$Data."\""."\n";
      file_put_contents($iniFname, $string, FILE_APPEND | LOCK_EX);
      
}
//print_r($csvArray);

file_put_contents($iniFname, "\n", FILE_APPEND | LOCK_EX);
$string = "[RPM_UNINSTALL_SECTION]"."\n";
file_put_contents($iniFname, $string, FILE_APPEND | LOCK_EX);
$numRPMsToUninstall = count($RPMsToUninstall);
for ($i=0; $i<$numRPMsToUninstall; $i++)
{
  $val = trim($RPMsToUninstall[$i]);
  $string= "uninstallRPMs[] = "."\""."rpmName=".$val."\""."\n";
  file_put_contents($iniFname, $string, FILE_APPEND | LOCK_EX);
}

if (!empty($ini_array['ota_commands_section_prerpm']['ota_exe_commands'])) {
    $otaExeCommands = $ini_array['ota_commands_section_prerpm']['ota_exe_commands'];
    file_put_contents($iniFname, "\n", FILE_APPEND | LOCK_EX);
    $string = "[OTA_COMMANDS_SECTION_PRE_RPM]"."\n";
    file_put_contents($iniFname, $string, FILE_APPEND | LOCK_EX);
    foreach ($otaExeCommands as $key => $data)
    {
        $string= "otaExeCmd[] = "."\""."cmdString=".$data."\""."\n";
        file_put_contents($iniFname, $string, FILE_APPEND | LOCK_EX);
    }
}

if (!empty($ini_array['ota_commands_section_postrpm']['ota_exe_commands'])) {
    $otaExeCommands = $ini_array['ota_commands_section_postrpm']['ota_exe_commands'];
    file_put_contents($iniFname, "\n", FILE_APPEND | LOCK_EX);
    $string = "[OTA_COMMANDS_SECTION_POST_RPM]"."\n";
    file_put_contents($iniFname, $string, FILE_APPEND | LOCK_EX);
    foreach ($otaExeCommands as $key => $data)
    {
        $string= "otaExeCmd[] = "."\""."cmdString=".$data."\""."\n";
        file_put_contents($iniFname, $string, FILE_APPEND | LOCK_EX);
    }
}

$totalFiles = $cntFiles + $numFiles;
$totalBytes = $sizePackage + ($sizeBytes / 1024);
$totalMBytes = number_format($totalBytes / 1024,0); 

file_put_contents($iniFname, "\n", FILE_APPEND | LOCK_EX);
$string = "[PARAMETERS_SECTION]"."\n";
file_put_contents($iniFname, $string, FILE_APPEND | LOCK_EX);

$manifestData = "otaType=".$ini_array['general_section']['ota_type'].'&'."fileCount=".$totalFiles.'&'."packageSize=".$totalMBytes.'MB'.'&'."encoding=".$ini_array['general_section']['encoding'].'&'."tarring=".$ini_array['general_section']['tarring'].'&'."golden=".$ini_array['general_section']['golden'].'&'."otaScriptsOnly=".$ini_array['general_section']['ota_scripts_only'].'&'."hybridMode=".$hybridMode.'&'."skip_request_to_download=".$ini_array['general_section']['skip_request_to_download']; 

$string= "otaParameters[] = "."\"".$manifestData."\""."\n";
file_put_contents($iniFname, $string, FILE_APPEND | LOCK_EX);

//encode manifest file
exec('openssl smime -encrypt -binary -aes-256-cbc -in '.$iniFname.' -out '.$enc_iniFname.' -outform DER '.$pemkey);

//http post server
$pieces= explode("-", $SWVersion);
$numberPieces = count($pieces);
$ReleaseDate = $pieces[0]."000000";

$SoftwarePlc = $ini_array['server_section']['sw_plc'];
$UrlManifestFile = $ini_array['server_section']['url_manifest_file'];
$SWOTAType = $ini_array['general_section']['ota_type'];
$SWRepoLink = $ini_array['server_section']['sw_repo_link'];
$SWCategory = $ini_array['server_section']['sw_category'];

$SWPostFields = array(
                    'SWVersion' => urlencode($SWVersion),
                    'ReleaseDate' => urlencode($ReleaseDate),
                    'SoftwarePlc' => urlencode($SoftwarePlc),
                    'UrlManifestFile' => urlencode($UrlManifestFile),
                    'SWOTAType' => urlencode($SWOTAType),
                    'SWRepoLink' => urlencode($SWRepoLink),
                    'SWCategory' => urlencode($SWCategory),
                );

echo "Values posted to server"."\n";
foreach ($SWPostFields as $key => $value) {
        echo"$key"."\t".$value."\n";
}

if (!($serverResponce=httpServerPost($SWPostFields))) {
     echo "Debug Message: server response not as expected". PHP_EOL;
}
$server_responce= json_decode($serverResponce, true);
//print_r($server_responce);
echo "Server response json decoded values"."\n";
foreach ($server_responce as $key => $value) {
        echo "$key"."\t".$value."\n";
}

foreach ($server_responce as $key => $value) {
  switch (trim($key)) {
     case "result":
         $server_result = trim($value);
         break;
     default:
         echo "ERROR: Server response key= $key, value= $value";
  }
}

if ($server_result == 'ACK') 
{  
    echo "Server response= ACK";

    //post rpms to server
    $rpmPath = $ini_array['general_section']['file_repo'];
    if ($handle = opendir($rpmPath)) {
       while (false !== ($entry = readdir($handle))) {
            $absolute_path = $rpmPath . $entry;
                   if ($entry != "." && $entry != ".." && (strtolower(substr($absolute_path, strrpos($absolute_path, '.') + 1)) == 'enc')) {
                       if (!is_dir($absolute_path)) {
                           exec('scp '.$absolute_path.' '.$ini_array['general_section']['scp_path']);   //unlink files, not folders
                       }
                   }
       }
       closedir($handle);
    }
}

$build_file_local_path = $ini_array['general_section']['build_file_local_path'];
$build_file_scp_path = $ini_array['general_section']['build_file_scp_path'];
exec('scp '.$build_file_local_path.' '.$build_file_scp_path);   

echo "\n";
echo "\n";
echo "==================== SCRIPT EXECUTION SUCCESSFUL =======================" . "\n";;

function processOTAScripts()
{
    global $filePrefix, $iniFname, $ini_array, $pemkey, $sizeBytes;

    $otaScripts = $ini_array['ota_files_section']['otascripts'];
    $otaScriptsPath = $ini_array['general_section']['local_ota_file_path'];
    $encodedOtaScriptsPath = $ini_array['general_section']['local_enc_ota_file_path']; 
    $eggOTAFilePath = $ini_array['general_section']['egg_ota_file_path'];
    $eggOTAFilePathEnc = $ini_array['general_section']['egg_ota_file_path_enc'];

    file_put_contents($iniFname, "\n", LOCK_EX);
    $string = "[OTA_FILE_SECTION]"."\n";
    file_put_contents($iniFname, $string, FILE_APPEND | LOCK_EX);

    foreach ($otaScripts as $key => $data)
    {
        $scriptFile = $otaScriptsPath.$data;  
        $md5string= exec('md5sum '.$scriptFile.'| cut -d" " -f1,3 | tr \' \' \',\'');
        $pieces_1= explode(",", $md5string);
        $numberPieces_1 = count($pieces_1);
        if ($numberPieces_1 > 2) {
            echo "error, number of pieces when processing md5string of $scriptFile file > 2";
            die();
        } else {
          $md5val= $pieces_1[0];
          $filePath= $pieces_1[1];
          $pieces_2= explode("/", $filePath);
          $numberPieces_2 = count($pieces_2);
          $fname = $pieces_2[$numberPieces_2-1];
        }
        $Data = "fileName=".$fname.'&'."md5=".$md5val.'&'."path=".$eggOTAFilePath;
        $string= "sourceOTAScript[] = "."\"".$Data."\""."\n";
        file_put_contents($iniFname, $string, FILE_APPEND | LOCK_EX);
      }
    
      file_put_contents($iniFname, "\n", FILE_APPEND | LOCK_EX);
      $string = "[ENCODED_OTA_FILE_SECTION]"."\n";
      file_put_contents($iniFname, $string, FILE_APPEND | LOCK_EX);

      foreach ($otaScripts as $key => $data)
      {
           $scriptFile = $otaScriptsPath.$data;  
           $enc_OTAFile = $encodedOtaScriptsPath . $filePrefix . $data . '.enc';
           
           exec('openssl smime -encrypt -binary -aes-256-cbc -in '.$scriptFile.' -out '.$enc_OTAFile.' -outform DER '.$pemkey);

           $md5string= exec('md5sum '.$enc_OTAFile.'| cut -d" " -f1,3 | tr \' \' \',\'');
           $pieces_1= explode(",", $md5string);
           $numberPieces_1 = count($pieces_1);
           if ($numberPieces_1 > 2) {
               echo "error, number of pieces when processing md5string of encoded $scriptFile file > 2";
               die();
           } else {
             $md5val= $pieces_1[0];
             $filePath= $pieces_1[1];
             $pieces_2= explode("/", $filePath);
             $numberPieces_2 = count($pieces_2);
             $fname = $pieces_2[$numberPieces_2-1];
           }
           $Data = "serverURL=".$ini_array['server_section']['server_url_path'].'&'."fileName=".$fname.'&'."md5=".$md5val.'&'."path=".$eggOTAFilePathEnc;
           $string= "encodedOTAScript[] = "."\"".$Data."\""."\n";
           file_put_contents($iniFname, $string, FILE_APPEND | LOCK_EX);
           //$sizeBytes = $sizeBytes + filesize($enc_OTAFile);

       }


} //function processOTAScripts

function processPatches()
{
    global $filePrefix, $iniFname, $ini_array, $pemkey, $cntFiles, $sizeBytes;

    if ((empty($ini_array['patch_files_section']['patch_file'])) || (empty($ini_array['patch_path_section']['patch_path'])))
    {
            echo "No Patch Files To Include or patch path empty" . "\n";
            return;
    } 

    $patchFiles = $ini_array['patch_files_section']['patch_file'];
    $patchFilesPath = $ini_array['general_section']['local_patch_path'];
    $encodedPatchPath = $ini_array['general_section']['local_enc_patch_path']; 
    $eggPatchFilePath = $ini_array['patch_path_section']['patch_path'];
    $eggPatchFilePathEnc = $ini_array['general_section']['egg_patch_file_path_enc'];

    file_put_contents($iniFname, "\n", FILE_APPEND | LOCK_EX);
    $string = "[PATCH_FILE_SECTION]"."\n";
    file_put_contents($iniFname, $string, FILE_APPEND | LOCK_EX);

    foreach ($patchFiles as $key => $data)
    {
        $patchFile = $patchFilesPath.$data;  
        $patchPath = $eggPatchFilePath[$key];  
        $md5string= exec('md5sum '.$patchFile.'| cut -d" " -f1,3 | tr \' \' \',\'');
        $pieces_1= explode(",", $md5string);
        $numberPieces_1 = count($pieces_1);
        if ($numberPieces_1 > 2) {
            echo "error, number of pieces when processing md5string of $scriptFile file > 2";
            die();
        } else {
          $md5val= $pieces_1[0];
          $filePath= $pieces_1[1];
          $pieces_2= explode("/", $filePath);
          $numberPieces_2 = count($pieces_2);
          $fname = $pieces_2[$numberPieces_2-1];
        }
        $Data = "fileName=".$fname.'&'."md5=".$md5val.'&'."path=".$patchPath;
        $string= "sourcePatchFile[] = "."\"".$Data."\""."\n";
        file_put_contents($iniFname, $string, FILE_APPEND | LOCK_EX);
        $cntFiles++;
      }
    
      file_put_contents($iniFname, "\n", FILE_APPEND | LOCK_EX);
      $string = "[ENCODED_PATCH_FILE_SECTION]"."\n";
      file_put_contents($iniFname, $string, FILE_APPEND | LOCK_EX);

      foreach ($patchFiles as $key => $data)
      {
           $patchFile = $patchFilesPath.$data;  
           $enc_PatchFile = $encodedPatchPath . $filePrefix . $data . '.enc';
           exec('openssl smime -encrypt -binary -aes-256-cbc -in '.$patchFile.' -out '.$enc_PatchFile.' -outform DER '.$pemkey);

           $md5string= exec('md5sum '.$enc_PatchFile.'| cut -d" " -f1,3 | tr \' \' \',\'');
           $pieces_1= explode(",", $md5string);
           $numberPieces_1 = count($pieces_1);
           if ($numberPieces_1 > 2) {
               echo "error, number of pieces when processing md5string of encoded $patchFile. file > 2";
           die();
           } else {
             $md5val= $pieces_1[0];
             $filePath= $pieces_1[1];
             $pieces_2= explode("/", $filePath);
             $numberPieces_2 = count($pieces_2);
             $fname = $pieces_2[$numberPieces_2-1];
           }
           $Data = "serverURL=".$ini_array['server_section']['server_url_path'].'&'."fileName=".$fname.'&'."md5=".$md5val.'&'."path=".$eggPatchFilePathEnc;
           $string= "encodedPatchFile[] = "."\"".$Data."\""."\n";
           file_put_contents($iniFname, $string, FILE_APPEND | LOCK_EX);
           $sizeBytes = $sizeBytes + filesize($enc_PatchFile); 

       }

} //function processPatches

function parseCSV($inputCSVFile) {

    global $filePrefix, $numFiles, $sizePackage, $SWVersion, $sourceFilesArray, $hybridMode;
    global $RPMsToUninstall;
    $i = 0;
    if (($fhandle = fopen($inputCSVFile, "r")) !== FALSE) {
      while(!feof($fhandle)) {
            $line_of_text = fgets($fhandle);

            $exploded_line_of_text = explode(",", $line_of_text); 

            if ($exploded_line_of_text[0] != 'UNINSTALL') {
                if ($i==0) $numFiles = trim($line_of_text);
                else if ($i==1) $sizePackage = trim($line_of_text);
                else if ($i==2) $versionRead = trim($line_of_text);
                else if ($i==3) $hybridMode = trim($line_of_text);
                else if ($i==4) $sourceFilesArray = array(trim($line_of_text));
                else array_push($sourceFilesArray, trim($line_of_text));
            } else {
                $RPMsToUninstall = array_slice($exploded_line_of_text, 1);;
                //print_r($RPMsToUninstall);
            }
            $i++;
       }
    } else {
       echo "Error opening input CSV file for reading"."\n";
    }

    array_pop($sourceFilesArray);

    $pieces= explode(".", $versionRead);
    $numberPieces = count($pieces);
    $SWVersion= $pieces[$numberPieces-1];
    $versionPlusFilePrefix = $SWVersion . "\n" . $filePrefix;
    //file_put_contents(BUILD_VERSION_FILE, $SWVersion);
    file_put_contents(BUILD_VERSION_FILE, $versionPlusFilePrefix);

    fclose($fhandle);
}


function httpServerPost($SWPostFields)
{
        global $productionOTA;
        $fields_string= ' ';
        //url-ify the data for the POST
        foreach($SWPostFields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
        //rtrim($fields_string, '&');
        trim($fields_string, '&');

        //open connection
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, SERVER_URL);
        if (!$productionOTA) {
		echo "Development Test" . PHP_EOL;
                curl_setopt($ch, CURLOPT_PROXY, SIMULATION_PROXY);  //url_setopt($ch, CURLOPT_PROXY, null); to disable proxy
        }
        //curl_setopt($ch, CURLOPT_PROXYUSERPWD, PROXYAUTH);
        curl_setopt($ch,CURLOPT_POST, count($SWPostFields));
        curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);

        //execute post
        $serverResponce = curl_exec($ch);
        //file_put_contents('C:\dev\egg\ota\server_response.txt', $serverResponce);
        //echo $serverResponce;

        //double check URL exists
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        //close connection
        curl_close($ch);

        if (!(($httpcode >= 200) && ($httpcode < 300))) {
                echo "Server URL [".SERVER_URL."] could not be found ". PHP_EOL;
                return false;
        }

        return $serverResponce;

} //function httpServerPost


function FileSizeConvert($bytes)
{
    $bytes = floatval($bytes);
        $arBytes = array(
            0 => array(
                "UNIT" => "TB",
                //"VALUE" => pow(1024, 4)
                "VALUE" => pow(1000, 4)
            ),
            1 => array(
                "UNIT" => "GB",
                //"VALUE" => pow(1024, 3)
                "VALUE" => pow(1000, 3)
            ),
            2 => array(
                "UNIT" => "MB",
                //"VALUE" => pow(1024, 2)
                "VALUE" => pow(1000, 2)
            ),
            3 => array(
                "UNIT" => "KB",
                //"VALUE" => 1024
                "VALUE" => 1000
            ),
            4 => array(
                "UNIT" => "B",
                "VALUE" => 1
            ),
        );

    foreach($arBytes as $arItem)
    {
        if($bytes >= $arItem["VALUE"])
        {
            $result = $bytes / $arItem["VALUE"];
            //$result = str_replace(".", "," , strval(round($result, 2)))." ".$arItem["UNIT"];
            $result = str_replace(".", "." , strval(round($result, 2)))." ".$arItem["UNIT"];
            break;
        }
    }
    return $result;
} //function FileSizeConvert


?>
