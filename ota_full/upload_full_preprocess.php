<?php
#DN Changes
#require_once("/usr/share/nginx/ota_packager_full/env_settings.php");
#$ini_array = parse_ini_file("/usr/share/nginx/ota_packager_full/ota_full.ini", true);
require_once("env_settings.php");
$ini_array = parse_ini_file("ota_full.ini", true);


$workDir = realpath(dirname(__FILE__));
$otaDir= date("YmdHis");
exec('mkdir '.$otaDir);

$pathotaDir = $workDir.'/'.$otaDir.'/';

$otaType = $ini_array['general_section']['ota_type'];
$buildDir = $ini_array['general_section']['build_file_path']; 
$tarredFilesDir = $ini_array['general_section']['file_repo'];
$pemkey = $ini_array['general_section']['pemkey'];
$csvFname = $ini_array['general_section']['csv_filename'];
$iniFname = $ini_array['general_section']['manifest_ini_file'];
$unzippedBuildFilesPath = $ini_array['general_section']['unzipped_build_files_path'];

if ($otaType == 'full') processBuildFile($buildDir, $tarredFilesDir, $pathotaDir, $pemkey, $csvFname, $otaType, $iniFname,
                                         $unzippedBuildFilesPath);


function processBuildFile($buildDir, $tarredFilesDir, $otaDir, $pemkey, $csvFname, $otaType, $iniFname,
                          $unzippedBuildFilesPath)
{

     global $ini_array, $sizePackage, $numFiles, $SWVersion;
 
     $fileCnt = 0;
     if ($handle = opendir($buildDir)) {
        while (false !== ($entry = readdir($handle))) {
              $absolute_path = $buildDir .  $entry;
                   if ($entry != "." && $entry != ".." && (strtolower(substr($absolute_path, strrpos($absolute_path, '.') + 1)) == 'hddimg'))            {
                         if (!is_dir($absolute_path)) {
                            $fileCnt++;
                            $buildFileName = $entry;
                            $abs_file_name = $absolute_path;
                            $sizePackage = filesize($absolute_path);
                         }
                   }
         }
     } else {
           echo "Error when trying to read build file"."\n";
           die();
     }

     if ($fileCnt != 1) {
         echo "Error: You should have only one file egg-image-valleyisland-64-BUILDDATE.hddimg in this directory. Please clean out folder= ".BUILD_FILE_PATH." and try again"."\n";
         die();
     }

     echo "Debug message: build file name = $buildFileName"."\n";
     echo "Debug message: build file size = $sizePackage"."\n";
     echo "Debug message: absolute file name = $abs_file_name"."\n";
     
     chdir($buildDir);
     #DN Change
     #exec('unzip '.$abs_file_name);
     //exec('unzip '.$buildFileName);

     #DN Change
     #exec('mv '.$abs_file_name.' '. $otaDir);
     //exec('mv '.$buildFileName.' '. $otaDir);
     chdir('../');
  
     file_put_contents($iniFname, "\n", LOCK_EX);
     $string = "[UNZIPPED_BUILD_MD5VAL_SECTION]"."\n";
     file_put_contents($iniFname, $string, FILE_APPEND | LOCK_EX);

     $sizePackage = 0;
     $numFiles = 0;
     if ($handle = opendir($buildDir)) {
        while (false !== ($entry = readdir($handle))) {
              $absolute_path = $buildDir .  $entry;
                   if ($entry != "." && $entry != ".." ) {
                      if (!is_dir($absolute_path)) {
                         $sizePackage = $sizePackage + filesize($absolute_path);
                         
                         //begin added section
                         $md5string= exec('md5sum '.$absolute_path.'| cut -d" " -f1,3 | tr \' \' \',\'');
                         $pieces_1= explode(',', $md5string);
                         $numberPieces_1 = count($pieces_1);
                         if ($numberPieces_1 > 2) {
                             echo "error, number of pieces when processing md5string of $absolute_path file > 2";
                             die();
                         } else {
                           $md5val= $pieces_1[0];
                           $filePath= $pieces_1[1];
                           $pieces_2= explode("/", $filePath);
                           $numberPieces_2 = count($pieces_2);
                           $fname = $pieces_2[$numberPieces_2-1];
                         }

                         if ($fname != 'manifest.txt')
                         {
                             $Data = "fileName=".$fname.'&'."md5=".$md5val.'&'."path=".$unzippedBuildFilesPath;
                             $string= "unzippedBuildMD5Vals[] = "."\"".$Data."\""."\n";
                             file_put_contents($iniFname, $string, FILE_APPEND | LOCK_EX);
                             echo "file path = $absolute_path \nfile name = $fname \nfile size = $sizePackage" . "\n";
                         }
                         //end added section

                         $numFiles++;
                         //echo $numFiles."\n";
                      }
                   }
         }
     } else {
           echo "Error when trying to determine total packages size"."\n";
           die();
     }

     echo "Debug message: total package size hddimg, manfiest.txt is = ".$sizePackage."\n";

     $version = getSWVersion($buildFileName);

     if ($version == null )
     {
        echo "Debug message: Error when reading Software Version"."\n";
        die();
     }

     echo "SWVersion: $version" . PHP_EOL;

     #DN add chdir
     chdir($buildDir);
     #unlink('manifest.txt');
     $zipCmd = "zip eota-update.zip $buildFileName manifest.txt";
     exec('echo "" > manifest.txt');
    
	
     exec($zipCmd);
  
     #DN add chdir
     chdir('../');


     //md5 of modified manifest file
     $manifestFname = 'manifest.txt';
     $absolute_path = $buildDir . $manifestFname; 
     $md5string= exec('md5sum '.$absolute_path.'| cut -d" " -f1,3 | tr \' \' \',\'');
     $pieces_1= explode(",", $md5string);
     $numberPieces_1 = count($pieces_1);
     if ($numberPieces_1 > 2) {
         echo "error, number of pieces when processing md5string of $absolute_path file > 2";
         die();
     } else {
         $md5val= $pieces_1[0];
         $filePath= $pieces_1[1];
         $pieces_2= explode("/", $filePath);
         $numberPieces_2 = count($pieces_2);
         $fname = $pieces_2[$numberPieces_2-1];
     }
     $Data = "fileName=".$fname.'&'."md5=".$md5val.'&'."path=".$unzippedBuildFilesPath;
     $string= "unzippedBuildMD5Vals[] = "."\"".$Data."\""."\n";
     file_put_contents($iniFname, $string, FILE_APPEND | LOCK_EX);

      if ($otaType == 'full') //delete file_repo folder 
      {
           if(!file_exists($ini_array['general_section']['file_repo'])) {
              mkdir($ini_array['general_section']['file_repo'], 0777, true);
           } else {
             if ($handle = opendir($ini_array['general_section']['file_repo'])) {
                  while (false !== ($entry = readdir($handle))) {
                      $absolute_path = $ini_array['general_section']['file_repo'] . $entry;
                           if ($entry != "." && $entry != ".." && (trim($entry) != trim($buildFileName))) {
                              if (!is_dir($absolute_path)) {
                                   unlink ($absolute_path);   //unlink files, not folders
                              }
                           }
                       }
                       closedir($handle);
                  }
           }
           //exec('cp -r '.$otaDir.'*.* '.$ini_array['general_section']['file_repo']);
           #DN Change
           #exec('cp eota-update.zip '.$ini_array['general_section']['file_repo']);
           exec('cp '.$buildDir.'eota-update.zip '.$ini_array['general_section']['file_repo']);
       }

     //exec('cp '.$buildDir.'eota-update.zip '.$otaDir);

     #DN Change
     chdir($ini_array['general_section']['file_repo']);
     //if ($handle = opendir($ini_array['general_section']['file_repo'])) {
     if ($handle = opendir("./")) {
         while (false !== ($entry = readdir($handle))) {
                 if (!is_dir($entry)) {
                      //exec('tar cz '.$absolute_path.' | split -b 8MiB - eota_update_split.tgz_');
                      exec('tar cz '.$entry.' | split -b 8MiB - eota_update_split.tgz_');
                 }
          }
      } else {
          echo "Error when tarring"."\n";
          die();
      }

      //DN Added
      chdir("../");

      exec('mv '.$ini_array['general_section']['file_repo'].'eota-update.zip'.' '.$otaDir);

    
      $fileCnt = 0;
      $fileArray = array();
      $directory = new DirectoryIterator($ini_array['general_section']['file_repo']);
      print_r($directory)."\n";
      
      foreach ($directory as $fileinfo) {
         if ($fileinfo->isFile()) {
             //if ($fileinfo->getExtension() == 'enc') {
             if ($fileinfo->getFilename() != 'eota-update.zip') {
                 $fileinfo->getFilename() . "\n";
                 array_push($fileArray,$fileinfo->getFilename());
                 $fileCnt++ . "\n";
             }
         }
      }

      // DN comment it out
      //$lenstr = strlen($buildFileName);
      //$versionInfo = substr($buildFileName, 0, $lenstr - 4);

      sort($fileArray);
      file_put_contents($csvFname, $fileCnt, LOCK_EX);
      file_put_contents($csvFname, "\n", FILE_APPEND | LOCK_EX);
      file_put_contents($csvFname, $sizePackage, FILE_APPEND | LOCK_EX);
      file_put_contents($csvFname, "\n", FILE_APPEND | LOCK_EX);
      //file_put_contents($csvFname, $SWVersion, FILE_APPEND | LOCK_EX);
      //file_put_contents($csvFname, $versionInfo, FILE_APPEND | LOCK_EX);
      file_put_contents($csvFname, $buildFileName, FILE_APPEND | LOCK_EX);
      file_put_contents($csvFname, "\n", FILE_APPEND | LOCK_EX);
      
      for ($i=0; $i<$fileCnt; $i++) {
          file_put_contents($csvFname, $fileArray[$i], FILE_APPEND | LOCK_EX);
          file_put_contents($csvFname, "\n", FILE_APPEND | LOCK_EX);
      }


     
}


function getSWVersion($fileName)
{
    global $SWVersion;

    $pieces= explode("-", $fileName);
    $numberPieces = count($pieces);

    $strEnd= $pieces[$numberPieces-1];

    $pieces= explode(".", $strEnd);
    $numberPieces = count($pieces);

    if ($numberPieces == 2) 
		$SWVersion = substr($pieces[0],0,8).'-1';
    else 
		$SWVersion = null;

    return $SWVersion;

}



?>

