#!/bin/sh

#DEVICE=/dev/sdb
#PART_PREFIX=""
#OUT=/dev/null
OUT=/home/root/ota_update_recovery.log
DEVICE=/dev/mmcblk0
PART_PREFIX="p"
DEBUG=0

#SOURCE_BOOTFS_MNT=$DEVICE${PART_PREFIX}1

#Image downloaded path
UPDATE_PACKAGE_PATH=/downloads/ota/image
UPDATE_PACKAGE_FOLDER=/downloads/ota
#FULL_UPDATE_PACKAGE=core-image-sato-valleyisland-64.hddimg
FULL_UPDATE_PACKAGE=$(ls -l /downloads/ota/image/ | awk '/egg-image/ {print $9}')
TMPDIR=/tmp
HDDIMG_MNT=$TMPDIR/hddimg
HDDIMG_ROOTFS_MNT=$TMPDIR/hddimg_rootfs
ROOTFS_MNT=$TMPDIR/rootfs
#BOOTFS_MNT=$TMPDIR/bootfs
#RECOVERY_ROOTFS_MNT=$TMPDIR/recovery_rootfs
#RECOVERY_ROOTFS=$DEVICE${PART_PREFIX}6
#BOOTFS=$DEVICE${PART_PREFIX}1
ROOTFS=$DEVICE${PART_PREFIX}6
FULL_PACKAGE_AVAILABLE=""

# Logging routines 
WARNINGS=0
ERRORS=0
CLEAR="$(tput sgr0)"
INFO="$(tput bold)"
RED="$(tput setaf 1)$(tput bold)"
GREEN="$(tput setaf 2)$(tput bold)"
YELLOW="$(tput setaf 3)$(tput bold)"

# Cleanup after die()
cleanup() {
        debug "Syncing and unmounting devices"
        # Unmount anything we mounteda
        cd ~
        umount $HDDIMG_ROOTFS_MNT || error "Failed to umount $HDDIMG_ROOTFS_MNT"
        umount $HDDIMG_MNT || error "Failed to umount $HDDIMG_MNT"
        umount $ROOTFS_MNT || error "Failed to umount $ROOTFS_MNT"
       # umount $RECOVERY_ROOTFS || error "Failed to umount $RECOVERY_ROOTFS"
        #umount $BOOTFS_MNT || error "Failed to umount $BOOTFS_MNT"

#	rm -rf $UPDATE_PACKAGE_PATH/*
}
info() {
        echo "${INFO}$1${CLEAR}"
}
error() {
        ERRORS=$((ERRORS+1))
        echo "${RED}$1${CLEAR}"
}
warn() {
        WARNINGS=$((WARNINGS+1))
        echo "${YELLOW}$1${CLEAR}"
}
success() {
        echo "${GREEN}$1${CLEAR}"
}
die() {
        error "$1"
        cleanup
        exit 1
}
debug() {
        if [ $DEBUG -eq 1 ]; then
                echo "$1"
        fi
}




updatefull() {
        info "Update HDDIMG"
#
# Installing to $DEVICE
#
info "Mounting images and device in preparation for installation"
mkdir -p $HDDIMG_MNT die "Failed to create $HDDIMG_MNT" 
mkdir -p $HDDIMG_ROOTFS_MNT die "Failed to create $HDDIMG_ROOTFS_MNT" 
#mkdir -p $BOOTFS_MNT die "Failed to create $BOOTFS_MNT" 
#mount -o loop $RECOVERY_ROOTFS_MNT/$UPDATE_PACKAGE_PATH/$FULL_UPDATE_PACKAGE $HDDIMG_MNT >$OUT 2>&1 || die "Failed to mount $FULL_UPDATE_PACKAGE on $HDDIMG_MNT"
mount -o loop $UPDATE_PACKAGE_PATH/$FULL_UPDATE_PACKAGE $HDDIMG_MNT >$OUT 2>&1 || die "Failed to mount $UPDATE_PACKAGE_PATH/$FULL_UPDATE_PACKAGE on $HDDIMG_MNT"
mkdir -p $ROOTFS_MNT die "Failed to create $ROOTFS_MNT" 
mount -t ext3 $ROOTFS $ROOTFS_MNT >$OUT 2>&1 || die "Failed to mount $ROOTFS_MNT"
if grep -q $ROOTFS /proc/mounts; then
        echo "download mounted no need to format"
else
        /sbin/mkfs.ext3 /dev/mmcblk0p5
        echo "formated to ext3"
	    mount -t ext3 $ROOTFS $ROOTFS_MNT >$OUT 2>&1 || die "Failed to mount $ROOTFS_MNT"
fi

#mount  $BOOTFS $BOOTFS_MNT >$OUT 2>&1 || die "Failed to mount $BOOTFS_MNT"
mount -o loop $HDDIMG_MNT/rootfs.img $HDDIMG_ROOTFS_MNT >$OUT 2>&1 || error "Failed to mount rootfs.img"
#rm -rf $ROOTFS_MNT/*
cd $ROOTFS_MNT
info "Delete ROOTFS files"
find -maxdepth 1 \! \( -name data -o -name . \) -exec rm -rf '{}' \;
cd -
info "Copying ROOTFS files (this may take a while)"
cp -a $HDDIMG_ROOTFS_MNT/* $ROOTFS_MNT/  >$OUT 2>&1 || die "Root FS copy failed"
#cp $HDDIMG_MNT/vmlinuz $BOOTFS_MNT >$OUT 2>&1 || error "Failed to copy vmlinuz"
#cd $BOOTFS_MNT/EFI/BOOT
#sed -i "s/mmcblk0p6/mmcblk0p5/" grub.cfg
success "Recovery update successfully"
cleanup
#info "Reboot the device"
sync
#reboot
}

#mkdir -p $RECOVERY_ROOTFS_MNT || die "Failed to create $RECOVERY_ROOTFS_MNT"
#debug "mounting $RECOVERY_ROOTFS on $RECOVERY_ROOTFS_MNT"
#mount $RECOVERY_ROOTFS $RECOVERY_ROOTFS_MNT >$OUT 2>&1 || error "Failed to mount $RECOVERY_ROOTFS on $RECOVERY_ROOTFS_MNT" 
#debug "checking $RECOVERY_ROOTFS/$UPDATE_PACKAGE_PATH/$PARTIAL_UPDATE_PACKAGE"


#debug "Checking $RECOVERY_ROOTFS_MNT/$UPDATE_PACKAGE_PATH/$FULL_UPDATE_PACKAGE "
#if [ -f "$RECOVERY_ROOTFS_MNT/$UPDATE_PACKAGE_PATH/$FULL_UPDATE_PACKAGE" ]
if [ -f "$UPDATE_PACKAGE_PATH/$FULL_UPDATE_PACKAGE" ]
then
    touch /home/root/ota_update_recovery.log
    updatefull
else
    error "Full update package not found"
    #sleep 5
    cleanup 
	#sed -i "s/mmcblk0p5/mmcblk0p6/" grub.cfg
    #reboot
fi 

      
