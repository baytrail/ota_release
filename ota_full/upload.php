<?php

#DN Change
#require_once("/usr/share/nginx/ota_packager_full/env_settings.php");
#$ini_array = parse_ini_file("/usr/share/nginx/ota_packager_full/ota_full.ini", true);
require_once("env_settings.php");
$ini_array = parse_ini_file("ota_full.ini", true);

$otaType = $ini_array['general_section']['ota_type'];

$cntFiles = 0; 
$sizeBytes = 0;

#echo MANIFEST_FILE;
#echo "\n";

if (file_exists(MANIFEST_FILE))
   parseCSV(MANIFEST_FILE);

echo "\nnumber of files = $numFiles\n";
echo "package size = $sizePackage\n";
echo "software version = $SWVersion\n";
echo "===========Input Files================"."\n";
print_r($sourceFilesArray);
echo "======================================"."\n";
echo $SWVersion."\n";


//print_r($ini_array);
//define('MANIFEST_FILE', 'manifest_file');
//define('BUILD_VERSION_FILE', 'build_version_file');
//define('SERVER_URL','server_http_URL');

$iniFname = $ini_array['general_section']['manifest_ini_file'];
$enc_iniFname = $ini_array['general_section']['enc_manifest_ini_file'];

$pemkey = $ini_array['general_section']['pemkey'];


//encoding
for ($i=0; $i<$numFiles; $i++)
{
      $infile = $ini_array['general_section']['file_repo'].$sourceFilesArray[$i];
      $outfile = $ini_array['general_section']['file_repo'].$sourceFilesArray[$i].'.enc';

      exec('openssl smime -encrypt -binary -aes-256-cbc -in '.$infile.' -out '.$outfile.' -outform DER '.$pemkey);

      if ($i==0) $encFileArray = array($outfile);
      else array_push($encFileArray, $outfile);
} 
//print_r($encFileArray);

//process OTA scripts
//------------------------------------------------------------------------------
processOTAScripts();
//------------------------------------------------------------------------------

//process patches 
//------------------------------------------------------------------------------
//if ($otaType == 'rpm') 
    processPatches();

//------------------------------------------------------------------------------

file_put_contents($iniFname, "\n", FILE_APPEND | LOCK_EX);
$string = "[SOURCE_FULL_FILE_SECTION]"."\n";
file_put_contents($iniFname, $string, FILE_APPEND | LOCK_EX);
for ($i=0; $i<$numFiles; $i++)
{
      $md5string= exec('md5sum '.$ini_array['general_section']['file_repo'].$sourceFilesArray[$i].'| cut -d" " -f1,3 | tr \' \' \',\'');
      $pieces_1= explode(",", $md5string);
      $numberPieces_1 = count($pieces_1);
      if ($numberPieces_1 > 2) {
          echo "error, number of pieces when processing md5string of $sourceFilesArray[$i] > 2";
          die();
      } else {
        $md5val= $pieces_1[0];
        $filePath= $pieces_1[1];
        $pieces_2= explode("/", $filePath);
        $numberPieces_2 = count($pieces_2);
        $fname = $pieces_2[$numberPieces_2-1];
      }
      $Data = "fileName=".$fname.'&'."md5=".$md5val;
      $string= "sourceFiles[] = "."\"".$Data."\""."\n";
      file_put_contents($iniFname, $string, FILE_APPEND | LOCK_EX);
}
      
file_put_contents($iniFname, "\n", FILE_APPEND | LOCK_EX);
$string = "[ENCODED_FULL_FILE_SECTION]"."\n"; 
file_put_contents($iniFname, $string, FILE_APPEND | LOCK_EX);
for ($i=0; $i<$numFiles; $i++)
{
      $md5string= exec('md5sum '.$encFileArray[$i].'| cut -d" " -f1,3 | tr \' \' \',\'');
      $pieces_1= explode(",", $md5string);
      $numberPieces_1 = count($pieces_1);
      if ($numberPieces_1 > 2) {
          echo "error, number of pieces when processing md5string of $encFileArray[$i]  > 2";
          die();
      } else {
        $md5val= $pieces_1[0];
        $filePath= $pieces_1[1];
        $pieces_2= explode("/", $filePath);
        $numberPieces_2 = count($pieces_2);
        $fname = $pieces_2[$numberPieces_2-1];
      }
      //$Data = "serverURL=".$ini_array['server_section']['server_url_path'].'&'."fileName=".$fname.'&'."md5=".$md5val.'&'."encoding=".$ini_array['general_section']['encoding'];
      $Data = "serverURL=".$ini_array['server_section']['server_url_path'].'&'."fileName=".$fname.'&'."md5=".$md5val;
      $string= "encodedFiles[] = "."\"".$Data."\""."\n";
      file_put_contents($iniFname, $string, FILE_APPEND | LOCK_EX);
      
}
//print_r($csvArray);

$totalFiles = $cntFiles + $numFiles;
if ($otaType == 'full') {
    $totalBytes = $sizePackage;
} else {
    $totalBytes = $sizePackage + $sizeBytes;
}
 
$totalMBytes = number_format($totalBytes / 1024 / 1024, 0); 

file_put_contents($iniFname, "\n", FILE_APPEND | LOCK_EX);
$string = "[PARAMETERS_SECTION]"."\n";
file_put_contents($iniFname, $string, FILE_APPEND | LOCK_EX);

$manifestData = "otaType=".$ini_array['general_section']['ota_type'].'&'."fileCount=".$totalFiles.'&'."packageSize=".$totalMBytes.'MB'.'&'."encoding=".$ini_array['general_section']['encoding'].'&'."tarring=".$ini_array['general_section']['tarring'].'&'."golden=".$ini_array['general_section']['golden'].'&'."otaScriptsOnly=".$ini_array['general_section']['ota_scripts_only'].'&'."hybridMode=".$ini_array['general_section']['hybrid_mode']; 

$string= "otaParameters[] = "."\"".$manifestData."\""."\n";
file_put_contents($iniFname, $string, FILE_APPEND | LOCK_EX);

//encode manifest file
exec('openssl smime -encrypt -binary -aes-256-cbc -in '.$iniFname.' -out '.$enc_iniFname.' -outform DER '.$pemkey);

//http post server
$pieces= explode("-", $SWVersion);
$numberPieces = count($pieces);
$ReleaseDate = $pieces[0]."000000";

$SoftwarePlc = $ini_array['server_section']['sw_plc'];
$UrlManifestFile = $ini_array['server_section']['url_manifest_file'];
$SWOTAType = $ini_array['general_section']['ota_type'];
$SWRepoLink = $ini_array['server_section']['sw_repo_link'];
$SWCategory = $ini_array['server_section']['sw_category'];

$SWPostFields = array(
                    'SWVersion' => urlencode($SWVersion),
                    'ReleaseDate' => urlencode($ReleaseDate),
                    'SoftwarePlc' => urlencode($SoftwarePlc),
                    'UrlManifestFile' => urlencode($UrlManifestFile),
                    'SWOTAType' => urlencode($SWOTAType),
                    'SWRepoLink' => urlencode($SWRepoLink),
                    'SWCategory' => urlencode($SWCategory),
                );

echo "Values posted to server"."\n";
foreach ($SWPostFields as $key => $value) {
        echo"$key"."\t".$value."\n";
}


// ***************************************************
// Send files to Chicken

if (!($serverResponce=httpServerPost($SWPostFields))) {
     echo "Server Response not OK"."\n";
}
$server_responce= json_decode($serverResponce, true);
//print_r($server_responce);
echo "Server response json decoded values"."\n";
foreach ($server_responce as $key => $value) {
        echo "$key"."\t".$value."\n";
}


foreach ($server_responce as $key => $value) {
  switch (trim($key)) {
     case "result":
         $server_result = trim($value);
         break;
     default:
         echo "Error: Server response key= $key, value= $value";
  }
}


if ($server_result == 'ACK') 
{  
    echo "Server response= ACK";

    //post rpms to server
    $rpmPath = $ini_array['general_section']['file_repo'];
    if ($handle = opendir($rpmPath)) {
       while (false !== ($entry = readdir($handle))) {
            $absolute_path = $rpmPath . $entry;
                   if ($entry != "." && $entry != ".." && (strtolower(substr($absolute_path, strrpos($absolute_path, '.') + 1)) == 'enc')) {
                       if (!is_dir($absolute_path)) {
                           exec('scp '.$absolute_path.' '.$ini_array['general_section']['scp_path']);   //unlink files, not folders
                       }
                   }
       }
       closedir($handle);
    }
}

$build_file_local_path = $ini_array['general_section']['build_file_local_path'];
$build_file_scp_path = $ini_array['general_section']['build_file_scp_path'];
exec('scp '.$build_file_local_path.' '.$build_file_scp_path);   

echo "\n";
echo "\n";
echo "==================== SCRIPT EXECUTION SUCCESSFUL =======================" . "\n";;

function processOTAScripts()
{
    global $iniFname, $ini_array, $pemkey, $sizeBytes;

    $otaScripts = $ini_array['ota_files_section']['otascripts'];
    $otaScriptsPath = $ini_array['general_section']['local_ota_file_path'];
    $encodedOtaScriptsPath = $ini_array['general_section']['local_enc_ota_file_path']; 
    $eggOTAFilePath = $ini_array['general_section']['egg_ota_file_path'];
    $eggOTAFilePathEnc = $ini_array['general_section']['egg_ota_file_path_enc'];

    file_put_contents($iniFname, "\n", FILE_APPEND | LOCK_EX);
    $string = "[OTA_FILE_SECTION]"."\n";
    file_put_contents($iniFname, $string, FILE_APPEND | LOCK_EX);

    foreach ($otaScripts as $key => $data)
    {
        $scriptFile = $otaScriptsPath.$data;  
        $md5string= exec('md5sum '.$scriptFile.'| cut -d" " -f1,3 | tr \' \' \',\'');
        $pieces_1= explode(",", $md5string);
        $numberPieces_1 = count($pieces_1);
        if ($numberPieces_1 > 2) {
            echo "error, number of pieces when processing md5string of $scriptFile file > 2";
            die();
        } else {
          $md5val= $pieces_1[0];
          $filePath= $pieces_1[1];
          $pieces_2= explode("/", $filePath);
          $numberPieces_2 = count($pieces_2);
          $fname = $pieces_2[$numberPieces_2-1];
        }
        $Data = "fileName=".$fname.'&'."md5=".$md5val.'&'."path=".$eggOTAFilePath;
        $string= "sourceOTAScript[] = "."\"".$Data."\""."\n";
        file_put_contents($iniFname, $string, FILE_APPEND | LOCK_EX);
      }
    
      file_put_contents($iniFname, "\n", FILE_APPEND | LOCK_EX);
      $string = "[ENCODED_OTA_FILE_SECTION]"."\n";
      file_put_contents($iniFname, $string, FILE_APPEND | LOCK_EX);

      foreach ($otaScripts as $key => $data)
      {
           $scriptFile = $otaScriptsPath.$data;  
           $enc_OTAFile = $encodedOtaScriptsPath.$data.'.enc'; 
           
           exec('openssl smime -encrypt -binary -aes-256-cbc -in '.$scriptFile.' -out '.$enc_OTAFile.' -outform DER '.$pemkey);

           $md5string= exec('md5sum '.$enc_OTAFile.'| cut -d" " -f1,3 | tr \' \' \',\'');
           $pieces_1= explode(",", $md5string);
           $numberPieces_1 = count($pieces_1);
           if ($numberPieces_1 > 2) {
               echo "error, number of pieces when processing md5string of encoded $scriptFile file > 2";
           die();
           } else {
             $md5val= $pieces_1[0];
             $filePath= $pieces_1[1];
             $pieces_2= explode("/", $filePath);
             $numberPieces_2 = count($pieces_2);
             $fname = $pieces_2[$numberPieces_2-1];
           }
           $Data = "serverURL=".$ini_array['server_section']['server_url_path'].'&'."fileName=".$fname.'&'."md5=".$md5val.'&'."path=".$eggOTAFilePathEnc;
           $string= "encodedOTAScript[] = "."\"".$Data."\""."\n";
           file_put_contents($iniFname, $string, FILE_APPEND | LOCK_EX);
           //$sizeBytes = $sizeBytes + filesize($enc_OTAFile);

       }


} //function processOTAScripts

function processPatches()
{
    global $iniFname, $ini_array, $pemkey, $cntFiles, $sizeBytes;

    if ((empty($ini_array['patch_files_section']['patch_file'])) || (empty($ini_array['patch_path_section']['patch_path'])))
    {
            echo "No Patch Files To Include or patch path empty" . "\n";
            return;
    } 

    $patchFiles = $ini_array['patch_files_section']['patch_file'];
    $patchFilesPath = $ini_array['general_section']['local_patch_path'];
    $encodedPatchPath = $ini_array['general_section']['local_enc_patch_path']; 
    $eggPatchFilePath = $ini_array['patch_path_section']['patch_path'];
    $eggPatchFilePathEnc = $ini_array['general_section']['egg_patch_file_path_enc'];

    file_put_contents($iniFname, "\n", FILE_APPEND | LOCK_EX);
    $string = "[PATCH_FILE_SECTION]"."\n";
    file_put_contents($iniFname, $string, FILE_APPEND | LOCK_EX);

    foreach ($patchFiles as $key => $data)
    {
        $patchFile = $patchFilesPath.$data;  
        $patchPath = $eggPatchFilePath[$key];  
        $md5string= exec('md5sum '.$patchFile.'| cut -d" " -f1,3 | tr \' \' \',\'');
        $pieces_1= explode(",", $md5string);
        $numberPieces_1 = count($pieces_1);
        if ($numberPieces_1 > 2) {
            echo "error, number of pieces when processing md5string of $scriptFile file > 2";
            die();
        } else {
          $md5val= $pieces_1[0];
          $filePath= $pieces_1[1];
          $pieces_2= explode("/", $filePath);
          $numberPieces_2 = count($pieces_2);
          $fname = $pieces_2[$numberPieces_2-1];
        }
        $Data = "fileName=".$fname.'&'."md5=".$md5val.'&'."path=".$patchPath;
        $string= "sourcePatchFile[] = "."\"".$Data."\""."\n";
        file_put_contents($iniFname, $string, FILE_APPEND | LOCK_EX);
        $cntFiles++;
      }
    
      file_put_contents($iniFname, "\n", FILE_APPEND | LOCK_EX);
      $string = "[ENCODED_PATCH_FILE_SECTION]"."\n";
      file_put_contents($iniFname, $string, FILE_APPEND | LOCK_EX);

      foreach ($patchFiles as $key => $data)
      {
           $patchFile = $patchFilesPath.$data;  
           $enc_PatchFile = $encodedPatchPath.$data.'.enc'; 
           exec('openssl smime -encrypt -binary -aes-256-cbc -in '.$patchFile.' -out '.$enc_PatchFile.' -outform DER '.$pemkey);

           $md5string= exec('md5sum '.$enc_PatchFile.'| cut -d" " -f1,3 | tr \' \' \',\'');
           $pieces_1= explode(",", $md5string);
           $numberPieces_1 = count($pieces_1);
           if ($numberPieces_1 > 2) {
               echo "error, number of pieces when processing md5string of encoded $patchFile. file > 2";
           die();
           } else {
             $md5val= $pieces_1[0];
             $filePath= $pieces_1[1];
             $pieces_2= explode("/", $filePath);
             $numberPieces_2 = count($pieces_2);
             $fname = $pieces_2[$numberPieces_2-1];
           }
           $Data = "serverURL=".$ini_array['server_section']['server_url_path'].'&'."fileName=".$fname.'&'."md5=".$md5val.'&'."path=".$eggPatchFilePathEnc;
           $string= "encodedPatchFile[] = "."\"".$Data."\""."\n";
           file_put_contents($iniFname, $string, FILE_APPEND | LOCK_EX);
           $sizeBytes = $sizeBytes + filesize($enc_PatchFile); 

       }

} //function processPatches

function parseCSV($inputCSVFile) {

    echo $inputCSVFile;
    global $numFiles, $sizePackage, $SWVersion, $sourceFilesArray;
    $i = 0;
    if (($fhandle = fopen($inputCSVFile, "r")) !== FALSE) {
      while(!feof($fhandle)) {
            $line_of_text = fgets($fhandle);

            if ($i==0) $numFiles = trim($line_of_text);
            else if ($i==1) $sizePackage = trim($line_of_text);
            else if ($i==2) $versionRead = trim($line_of_text);
            else if ($i==3) $sourceFilesArray = array(trim($line_of_text));
            else array_push($sourceFilesArray, trim($line_of_text));
            $i++;
       }
    } else {
       echo "Error opening input CSV file for reading"."\n";
    }

    array_pop($sourceFilesArray);

    // DN call fn to get version instead
    //$pieces= explode(".", $versionRead);
    //$numberPieces = count($pieces);
    //$SWVersion= $pieces[$numberPieces-1];
    $SWVersion = getSWVersion($versionRead);

    file_put_contents(BUILD_VERSION_FILE, $SWVersion);

    fclose($fhandle);
}

function getSWVersion($fileName)
{
    global $SWVersion;

    $pieces= explode("-", $fileName);
    $numberPieces = count($pieces);

    $strEnd= $pieces[$numberPieces-1];

    $pieces= explode(".", $strEnd);
    $numberPieces = count($pieces);

    if ($numberPieces == 2) 
		$SWVersion = substr($pieces[0],0,8).'-1';
    else 
		$SWVersion = null;

    return $SWVersion;

}

function httpServerPost($SWPostFields)
{
        global $productionOTA;
        $fields_string= ' ';
        //url-ify the data for the POST
        foreach($SWPostFields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
        //rtrim($fields_string, '&');
        trim($fields_string, '&');

        //open connection
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, SERVER_URL);
        if (!$productionOTA) {
                curl_setopt($ch, CURLOPT_PROXY, SIMULATION_PROXY);  //url_setopt($ch, CURLOPT_PROXY, null); to disable proxy
        }
        //curl_setopt($ch, CURLOPT_PROXYUSERPWD, PROXYAUTH);
        curl_setopt($ch,CURLOPT_POST, count($SWPostFields));
        curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);

        //execute post
        $serverResponce = curl_exec($ch);
        //file_put_contents('C:\dev\egg\ota\server_response.txt', $serverResponce);
        //echo $serverResponce;

        //double check URL exists
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        //close connection
        curl_close($ch);

        if (!(($httpcode >= 200) && ($httpcode < 300))) {
                echo "Server URL [".SERVER_URL."] could not be found "."\n";
                return false;
        }

        return $serverResponce;

} //function httpServerPost


function FileSizeConvert($bytes)
{
    $bytes = floatval($bytes);
        $arBytes = array(
            0 => array(
                "UNIT" => "TB",
                //"VALUE" => pow(1024, 4)
                "VALUE" => pow(1000, 4)
            ),
            1 => array(
                "UNIT" => "GB",
                //"VALUE" => pow(1024, 3)
                "VALUE" => pow(1000, 3)
            ),
            2 => array(
                "UNIT" => "MB",
                //"VALUE" => pow(1024, 2)
                "VALUE" => pow(1000, 2)
            ),
            3 => array(
                "UNIT" => "KB",
                //"VALUE" => 1024
                "VALUE" => 1000
            ),
            4 => array(
                "UNIT" => "B",
                "VALUE" => 1
            ),
        );

    foreach($arBytes as $arItem)
    {
        if($bytes >= $arItem["VALUE"])
        {
            $result = $bytes / $arItem["VALUE"];
            //$result = str_replace(".", "," , strval(round($result, 2)))." ".$arItem["UNIT"];
            $result = str_replace(".", "." , strval(round($result, 2)))." ".$arItem["UNIT"];
            break;
        }
    }
    return $result;
} //function FileSizeConvert


?>
