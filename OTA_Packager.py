#!/usr/bin/python
#  Prerequisites:
#	1. Script shall be run in the Ubuntu host machine where source code is cloned & build is made.
#	2. There should not be any local source code changes (i.e. no uncommitted changes in the downloaded repositories).
#	3. The script should be run from user account which has permissions to read from 
#	   /root/GBS-ROOT/local/repos/tizen2.0mobile/i586/RPMS/ and create files/directories in PWD.
#
#This script:
# -> The script takes two git-tags as input. All commits between the two tags will be part of OTA package.
# -> Run script as: ./OTA_Packager.py  tag1 tag2
#	 -> tag1 and tag2 must exist on the same git-branch.
#	 -> identifies git projects and checks which files/projects have been modified between the specified git-tags
#	 -> If there is change to any file in the repository, its corresponding .spec file is used to find out which RPM has to be shortlisted for OTA update.
# -> This script gives:
#	-> a text file (eota_manifest.csv) as output. The text file shall contain names of RPM files that will have to be part of incremental OTA update.
#	-> a zip file (update.zip) that comprizes of all the RPMs that need to be sent to remote device O-T-A.
#
# NOTE: EXPLICIT_REMOVE_PACKAGES_FOR_OTA lists all the packages that the .spec file promises to generate, but
# 		does not do so (because no files are listed under %files section of the .spec file).
#
#	@author: Vasantha Kumar V (vasantha.kumarx.v@intel.com)
#
##VERBOSE levels:
# 0 -> Mandatory notifications, Errors, Warnings
# 1 -> Manjor information, progress notifications
# 2 -> Value of variables
# 3 -> debug information, output from loops
# 4 -> Function entry-exit logs

import os as os_api
import subprocess as subp_api
import re as reg_ex
import sys as sys_api
import string
import time
import smtplib
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart

######### Begin: Configurable parameters...
VERBOSE=2
EGG_GIT_REPO="/root/egg_git_repo/"
BUILD_BASE="/root/rpmbuild/BUILD/"
RPM_DIR="/root/GBS-ROOT/local/repos/tizen2.0mobile/i586/RPMS/"
BUILD_ITERATION_NUMBER_FILE="/root/BuildIterationNumber.txt"
OTA_ZIP_FILENAME = "update.zip"
OUTPUT_MANIFEST_FILE="eota_manifest.csv"

EGG_REPOSITORIES=[] #Iterate through 'EGG_GIT_REPO' dirs and populate.
SECIAL_REPOS=["the_egg-platform", "the_egg-tools", "the_egg-third-party", "the_egg-preos"]
#UNINSTALL_PACKAGES_THROUGH_OTA: comma separated rpm pkg to uninstall over the air. Adds [UNINSTAL,] line to eota_manifest.csv
UNINSTALL_PACKAGES_THROUGH_OTA = [] #example: ["osp-connectivity-service"]
EXPLICIT_ADD_PACKAGES_FOR_OTA=[]
EXPLICIT_REMOVE_PACKAGES_FOR_OTA=[]
EXPLICIT_IGNORE_PLATFORM_PROJECT=[] #comma separated list of projects to ignore, ex: ["aiccu"]
EXPLICIT_IGNORE_SPEC_FILE=[] #comma separated list of spec files to ignore, ex: ["aiccu.spec"]
######### End: Configurable parameters...


### The following variables will be set in "initialize()" function below.
#NOTE: PROJECTS_WITH_CHANGES_FILE gets prefixed with "OUT_DIR" in 'initialize()' function.
SEND_EMAILS = True
SMTP_SERVER = "smtp.intel.com"
PROJECTS_WITH_CHANGES_FILE="projects_for_ota.txt"
GIT_TAG_START=""
GIT_TAG_END=""
OUT_DIR=""
ADDITIONAL_OUT_DIR_BASE="/root/OTA_UPDATE/"
MNFST_FILE=""
TMP_MNFST_FILE=""
HYBRID_PARAMS=""
FULLBUILD_ZIPFILE=""

#Get current date & time
process=subp_api.Popen("date +%Y%m%d",stdout=subp_api.PIPE,stderr=subp_api.PIPE,shell=True)
output,error=process.communicate()
CURR_DATE=output[0:8] # in "YYYYMMDD" format
process=subp_api.Popen("date +%H%M%S",stdout=subp_api.PIPE,stderr=subp_api.PIPE,shell=True)
output,error=process.communicate()
CURR_TIME=output[0:6] # in "HHMMSS" format

LOG_PATH="/var/log/OTA_Packager/"
LOG_FILE = LOG_PATH+"OTAPkgr_"+CURR_DATE+"_"+CURR_TIME+".log"
print "Logs saved to: "+LOG_FILE

SYNTAX = "Syntax: "+sys_api.argv[0]+"  git_tag_start   git_tag_end   LifePlatform_ZipFile\n"

################### Duplicate STDOUT prints to file #########################
class Tee(object):
	def __init__(self, name, mode="a"):
		if (not os_api.path.isdir(LOG_PATH)): os_api.makedirs(LOG_PATH)
		self.file = open(name, mode)
		self.stdout = sys_api.stdout
		sys_api.stdout = self
	def __del__(self):
		sys_api.stdout = self.stdout
		self.file.flush()
		self.file.close()
	def write(self, data):
		self.file.write(data)
		self.file.flush()
		self.stdout.write(data)

Tee(LOG_FILE)
###############################################################################

if (len(sys_api.argv) == 1):
	print "Identify packages needed for Over-The-Air upgrade of EGG devices."
	print SYNTAX
	sys_api.exit(0)

###############################################################################
###############################################################################

# Function: my_send_mail
# This function sends an email with attachment. Message body can be specified 
# as a string argument or a file contents or both.
# Pre-requisites:
#	SMTP_SERVER should be defined to have correct smtp address of mail server.
#	CURR_DATE & CURR_TIME should be defined (will be added to subject line & msg body.
# Parameters:
# 	mail_from: From address of the sender of email. Ex: "tizen-bot-vasanth@intel.com"
#	mail_to: To addresses of recepients(comma separated) to send the email.
#	         Ex: "vasantha.kumarx.v@intel.com, melanathurx.narasimham@intel.com"
#	mail_cc: CC addresses of recepients of email. Ex: "vasantha.kumar@wipro.com"
#	mail_subject: Subject line for the email. Ex: "This is subject line"
#	mail_body_text: String containing multiple lines of email message body. Ex: "Message body line1\nThis is line2\n"
#	mail_body_file: Filename that contains the message body. Ex: "./textfile.txt"
#	mail_attachment: Filename that has to be attached to the email: ex: "./attachment.txt"
# Returns: 0 on success. -ve value on failure.
def my_send_mail(mail_from, mail_to, mail_cc, mail_subject, mail_body_text, mail_body_file, mail_attachment):
	MAIL_VERBOSITY=0

	if (False == SEND_EMAILS): return 0

	#Get current date & time
	process=subp_api.Popen("date +%Y%m%d",stdout=subp_api.PIPE,stderr=subp_api.PIPE,shell=True)
	output,error=process.communicate()
	CURR_DATE=output[0:8]
	process=subp_api.Popen("date +%H%M%S",stdout=subp_api.PIPE,stderr=subp_api.PIPE,shell=True)
	output,error=process.communicate()
	CURR_TIME=output[0:6]

	is_msgbodyfile_exists = False
	msgbodyfile_size = 0
	is_attachmtfile_exists = False
	attachmtfile_size = 0
	if os_api.access(mail_body_file, os_api.R_OK):
		is_msgbodyfile_exists = True
		msgbodyfile_size = os_api.path.getsize(mail_body_file)
	if os_api.access(mail_attachment, os_api.R_OK):
		is_attachmtfile_exists = True
		attachmtfile_size = os_api.path.getsize(mail_attachment)

	if (MAIL_VERBOSITY >= 2):
		print "E-mail parameters..."
		print "\tFROM: "+mail_from
		print "\tTO: "+mail_to
		print "\tCC: "+mail_cc
		print "\tSUBJECT: "+mail_subject
		print "\tBody-text: "+mail_body_text
		if (is_msgbodyfile_exists):	print "\tBody-file: "+mail_body_file+", Size="+str(msgbodyfile_size)+" bytes"
		else:	print "\tBody-file: "+mail_body_file+" (DOES NOT EXIST!)"
		if (is_attachmtfile_exists):	print "\tAttachment-file: "+mail_attachment+", Size="+str(attachmtfile_size)+" bytes"
		else:	print "\tAttachment-file: "+mail_attachment+" (DOES NOT EXIST!)"
		print "\t----------------------------------------\n"

	if (MAIL_VERBOSITY >= 2): print "Reading file (for msg body): "+mail_body_file
	msg_template="Date: "+CURR_DATE+"\nTime: "+CURR_TIME+"\n"
	msg_template=msg_template+"~~~~~~~~~~~~~Message body:text~~~~~~~~~~~~~~~~\n"+mail_body_text
	if (is_msgbodyfile_exists):
		msg_template=msg_template+"\n\n~~~~~~~~~~~~~Message body:file~~~~~~~~~~~~~~~~\n"
		fp = open(mail_body_file, 'r') # Create a text/plain message
		body_contents = fp.read()
		fp.close()
	else: body_contents = "\n\n~~~~~~~~~~~~~End of Message body:text~~~~~~~~~~~~~~~~\n"

	mail_msg = MIMEMultipart()

	mail_msg['From'] = mail_from
	mail_msg['To'] = mail_to
	mail_msg['Cc'] = mail_cc
	#mail_msg['Reply-to'] = "vasantha.kumarx.v@intel.com" #not required, usually
	mail_msg['Subject'] = "["+CURR_DATE+":"+CURR_TIME+"] "+mail_subject

	# That is what u see if dont have an email reader:
	mail_msg.preamble = 'Multipart massage.\n'

	# This is the textual part:
	part = MIMEText(msg_template + body_contents)
	mail_msg.attach(part)

	# This is the binary part(The Attachment):
	if (is_attachmtfile_exists):
		part = MIMEApplication(open(mail_attachment,"r").read())
		part.add_header('Content-Disposition', 'attachment', filename=mail_attachment)
		mail_msg.attach(part)

	# Send the message via our own SMTP server, but don't include the envelope header.
	print "Sending email ..."
	try:
		if (MAIL_VERBOSITY >= 2): print "\tCalling smtplib.SMTP("+SMTP_SERVER+") ..."
		s = smtplib.SMTP(SMTP_SERVER)
		if (MAIL_VERBOSITY >= 2): print "\tCalling s.sendmail() ..."
		y=s.sendmail(mail_from, mail_to.split(","), mail_msg.as_string())
	except Exception as exc:
		print "ERROR! Cannot send email.\nsmtplib.SMTP('"+SMTP_SERVER+"') or sendmail() raised exception!\n", exc
		return -500
	if (MAIL_VERBOSITY >= 1): print "\ts.sendmail returned:", y
	if (MAIL_VERBOSITY >= 2): print "Sleeping for 3 seconds..."
	time.sleep(3) # delays for 3 seconds
	s.quit()

	return 0




def my_exit(code, msg):
	if (VERBOSE>=4): print "Entering function "+sys_api._getframe().f_code.co_name+"()"

	mail_body = "Command: " + " ".join(sys_api.argv) + "\n\n"
	if (code < 0):
		mail_subject = "ERROR: "+sys_api.argv[0]+" failed!"
		mail_body = mail_body+mail_subject+"\n\nReason:["+msg+"]. code="+str(code)+ \
					"\n\nFailed to prepare over-the-air update package. Check attachment for detailed logs.\n"
	elif (code > 0):
		mail_subject = "Success?: "+sys_api.argv[0]+"completed."
		mail_body = mail_body+mail_subject+"\n\n Reason:["+msg+"]. code="+str(code)+ \
					"\n\nPrepared over-the-air update package. Check attachment for detailed logs.\n"
	else:
		mail_subject = sys_api.argv[0]+" Success."
		mail_body = mail_body+sys_api.argv[0]+" success.\n"
	mail_body = mail_body+"\n\nBest Regards,\n"+sys_api.argv[0]

	# my_send_mail(mail_from, mail_to, mail_cc, mail_subject, mail_body_text, mail_body_file, mail_attachment):
	if (0 != my_send_mail("support@eggcyte.com", #FROM address
				"jdnevarez@hotmail.com, tsmartis@yahoo.com", # TO addresses (comma separated)
				"",                                     # CC
				mail_subject,                           # Email SUBJECT
				mail_body,                              # Email body
				"",                                     # Email body
				LOG_FILE)):                             # Email attachment
		print "ERROR: Failed to send email!"

	sys_api.exit(code)



def error_exit_initialize():
	if (VERBOSE>=4): print "Entering function "+sys_api._getframe().f_code.co_name+"()"
	print "ERROR: Initialization failed!"
	my_exit(-400, "initialize() failed")
	if (VERBOSE>=4): print "Returning from function "+sys_api._getframe().f_code.co_name+"()"


def error_exit_checktag():
	if (VERBOSE>=4): print "Entering function "+sys_api._getframe().f_code.co_name+"()"
	print "ERROR: Checking for git tag failed!"
	my_exit(-400, "check_git_tag() failed")
	if (VERBOSE>=4): print "Returning from function "+sys_api._getframe().f_code.co_name+"()"

def error_exit_checkrepo4changes():
	if (VERBOSE>=4): print "Entering function "+sys_api._getframe().f_code.co_name+"()"
	print "ERROR: Failed to check repositories for changes!"
	my_exit(-400, "check_repo_for_changes() failed")
	if (VERBOSE>=4): print "Returning from function "+sys_api._getframe().f_code.co_name+"()"

def error_exit_identifyrpm4ota():
	if (VERBOSE>=4): print "Entering function "+sys_api._getframe().f_code.co_name+"()"
	print "ERROR: Failed to identify RPMs for OTA upgrade!"
	my_exit(-400, "identify_rpms_for_ota() failed")
	if (VERBOSE>=4): print "Returning from function "+sys_api._getframe().f_code.co_name+"()"

def error_exit_prepareotapackage():
	if (VERBOSE>=4): print "Entering function "+sys_api._getframe().f_code.co_name+"()"
	print "ERROR: Failed to prepare OTA package!"
	my_exit(-400, "prepare_ota_package() failed")
	if (VERBOSE>=4): print "Returning from function "+sys_api._getframe().f_code.co_name+"()"





# This function:
#	- validates command line parameters.
#	- initializes variables.
#	- identifies git repositories in the given path
#	- returns 0 on success. -ve value otherwise.
def initialize():
	global GIT_TAG_START
	global GIT_TAG_END
	global OUT_DIR
	global PROJECTS_WITH_CHANGES_FILE
	global EGG_REPOSITORIES
	global MNFST_FILE
	global TMP_MNFST_FILE
	global FULLBUILD_ZIPFILE
	if (VERBOSE>=4): print "Entering function "+sys_api._getframe().f_code.co_name+"()"

	if (VERBOSE>=2): print "Command: "+" ".join(sys_api.argv)

	is_error = 0
	if (len(sys_api.argv) != 4):
		print "ERROR! Invalid arguments.\n"+SYNTAX
		my_exit(-500, "Invalid arguments")

	#Configuration/input validation
	if (not os_api.path.isdir(EGG_GIT_REPO)):
		print "ERROR: git repository path not found: "+EGG_GIT_REPO
		is_error = -1
	if (not os_api.path.isdir(BUILD_BASE)):
		print "ERROR: Build base directory not found: "+BUILD_BASE
		is_error = -1
	if (not os_api.path.isdir(RPM_DIR)):
		print "ERROR: RPM directory not found: "+RPM_DIR
		is_error = -1

	GIT_TAG_START=sys_api.argv[1]
	GIT_TAG_END=sys_api.argv[2]
	###STITCH..BIN handling: Begin
	if (len(sys_api.argv) == 3): FULLBUILD_ZIPFILE = ""
	else: FULLBUILD_ZIPFILE = os_api.path.abspath(sys_api.argv[3])
	###STITCH..BIN handling: End
	OUT_DIR=EGG_GIT_REPO+"OTA_Package_"+GIT_TAG_START+"_"+GIT_TAG_END+"/"
	PROJECTS_WITH_CHANGES_FILE = OUT_DIR+PROJECTS_WITH_CHANGES_FILE

	if (os_api.path.isdir(OUT_DIR)):
		print "Error: directory '"+OUT_DIR+"' already exists!"
		print " ===> Rename or remove the directory: "+OUT_DIR
		is_error = -1

	if ("*" in GIT_TAG_START):
			print "Invalid start tag: "+GIT_TAG_START
			is_error = -1
	if ("*" in GIT_TAG_END):
			print "Invalid end tag: "+GIT_TAG_END
			is_error = -1

	if (not is_error):
		#Traverse the 'EGG_GIT_REPO' and find out how many repositories are present
		os_api.chdir(EGG_GIT_REPO)
		if (VERBOSE>=1): print "INFO: Looking for repositories in: "+EGG_GIT_REPO
		for root, dirs, files in os_api.walk(EGG_GIT_REPO):
			if (VERBOSE>=2): print "Root: ",root
			if (VERBOSE>=2): print "Dirs: ",dirs
			if (VERBOSE>=2): print "Files: ",files
			for dir in dirs:
				if os_api.access(dir+"/.git/HEAD", os_api.R_OK):
					EGG_REPOSITORIES.append(dir)
					if (VERBOSE>=2): print "INFO: Found git repository: "+dir
				else:
					if (VERBOSE>=1): print "INFO: Folder '"+dir+"' is not git repository. Skipping..."
			break

		print "#####################################################"
		print "\tgit repo path: "+EGG_GIT_REPO
		print "\tStart git-tag: "+GIT_TAG_START
		print "\tEnd git-tag: "+GIT_TAG_END
		print "\tRepositories:",EGG_REPOSITORIES
		print "#####################################################"

		MNFST_FILE = OUT_DIR+OUTPUT_MANIFEST_FILE
		TMP_MNFST_FILE = MNFST_FILE+".tmp"

		if (VERBOSE>=2): print "INFO: Creating directory - "+OUT_DIR
		os_api.mkdir(OUT_DIR)
	if (VERBOSE>=4): print "Returning from function "+sys_api._getframe().f_code.co_name+"()"
	return is_error


# This function:
#	- checks if tagA and tagB are present in the given repository.
#	- returns 0 if both tags are present. -ve value otherwise.
def check_git_tag_in_repo(repo, start_tag, end_tag):
	saved_pwd = os_api.getcwd() # Save PWD
	is_error = 0
	os_api.chdir(EGG_GIT_REPO+repo)
	CMD = "git tag --list "
	CMD_START = CMD + start_tag
	if (VERBOSE==1): print "\t"+repo+"... ",
	elif (VERBOSE>=1): print "\tChecking for git tag (start) '"+start_tag+"' in repo: "+repo
	process=subp_api.Popen(CMD_START, stdout=subp_api.PIPE, stderr=subp_api.PIPE, shell=True)
	output,error=process.communicate()
	if (VERBOSE>=2): print "\tCommand: "+CMD_START
	if (VERBOSE>=3): print "\t\tstdout: '"+output+"'"
	if (VERBOSE>=3): print "\t\tstderr: '"+error+"'"
	if error:
		print "\nError! "+CMD_START+" failed.\n" + error
		print "Error while checking for git start-tag '"+start_tag+"' in '" + repo +"' repository!"
		is_error = -1
	else:
		tag_as_list = output.split()
		tag_as_list = [x for x in tag_as_list if x] # Remove empty list entries
		if (1 != len(tag_as_list)):
			print "\nError! git start-tag '"+start_tag+"' is not found in '" + repo +"' repository!"
			print "Tip: Is '"+repo+"' a new repository ? Try applying tag '"+start_tag+"' to first commit."
			is_error = -1
		else:
			if (VERBOSE==1): print "Start-tag found.",
	CMD_END = CMD + end_tag
	if (VERBOSE==1): pass
	elif (VERBOSE>=1): print "\tChecking for git tag (end) '"+end_tag+"' in repo: "+repo
	process=subp_api.Popen(CMD_END, stdout=subp_api.PIPE, stderr=subp_api.PIPE, shell=True)
	output,error=process.communicate()
	if (VERBOSE>=2): print "\tCommand: "+CMD_END
	if (VERBOSE>=3): print "\t\tstdout: '"+output+"'"
	if (VERBOSE>=3): print "\t\tstderr: '"+error+"'"
	if error:
		print "\nError! "+CMD_END+" failed.\n" + error
		print "Error while checking for git end-tag '"+end_tag+"' in '" + repo +"' repository!"
		is_error = -1
	else:
		tag_as_list = output.split()
		tag_as_list = [x for x in tag_as_list if x] # Remove empty list entries
		if (1 != len(tag_as_list)):
			print "Error! git end-tag '"+end_tag+"' not found in '" + repo +"' repository! \nCannot proceed!"
			is_error = -1
		else:
			if (VERBOSE==1): print " End-tag found."

	os_api.chdir(saved_pwd)
	if ((not is_error) and (VERBOSE>=2)): print "\tINFO: Tags "+start_tag+" & "+end_tag+" found in repository '"+repo+"'"
	return is_error



# This function:
#	- Iterates through each of repositories in EGG_GIT_REPO.
#	- invokes check_git_tag_in_repo(repo, tagA, tagB) to check for tags
def check_git_tag():
	if (VERBOSE>=4): print "Entering function "+sys_api._getframe().f_code.co_name+"()"

	is_error = 0
	tag_err_list = []
	if (VERBOSE>=1): print "Checking for git tags in repositories..."
	for repo in EGG_REPOSITORIES:
		# Check whether git-tag is present in the repo
		if (0 != check_git_tag_in_repo(repo, GIT_TAG_START, GIT_TAG_END)):
			tag_err_list.append(repo)
			is_error = -1

	if (is_error and (len(tag_err_list)>0)): print "\nFailed to locate tags in repositories:",tag_err_list
	if (VERBOSE>=4): print "Returning from function "+sys_api._getframe().f_code.co_name+"()"
	return is_error


# This function:
#	- Iterates through each of repositories in EGG_GIT_REPO.
#	- Runs "git log tagA..tagB" command in each repo and creates file - repo+"_changes.txt"
def check_repo_for_changes():
	if (VERBOSE>=4): print "Entering function "+sys_api._getframe().f_code.co_name+"()"

	is_error = 0
	''' git log --name-status --pretty=oneline jun_7..aug_8    <<<< Recommended(Shows prefix A/M/D)
		git log --name-only aug_5..aug_6 --pretty=oneline
		git log --stat aug_5..aug_6 --pretty=oneline '''
	if (VERBOSE>=1): print "Checking for git commits in repositories..."
	for repo in EGG_REPOSITORIES:
		if (VERBOSE>=2):
			txt = " %s " % repo
			print "\t"+txt.center(80, "=")
		os_api.chdir(EGG_GIT_REPO+repo)
		if (VERBOSE==1): print "\tChecking for git commits in repo: "+repo+" ..... ",
		elif (VERBOSE>=1): print "\tChecking for git commits in repo: "+repo
		CMD = "git log --name-status --pretty=oneline "+GIT_TAG_START+".."+GIT_TAG_END
		process=subp_api.Popen(CMD, stdout=subp_api.PIPE, stderr=subp_api.PIPE, shell=True)
		output,error=process.communicate()
		if (VERBOSE>=2): print "\tCommand: "+CMD
		if (VERBOSE>=3): print "\t\tstdout: '"+output+"'"
		if (VERBOSE>=3): print "\t\tstderr: '"+error+"'"
		if error:
			print "Error! "+CMD+" failed.\n" + error
			print "Error while checking for commits between git start-tag '"+GIT_TAG_START+"' and end-tag '"+GIT_TAG_END+"' in '" + repo +"' repository!"
			is_error = -1
		else:
			# If there is any logs, no need to parse! Mark this project for OTA update
			if (len(output) > 0):
				try:
					proj_ptr = open(PROJECTS_WITH_CHANGES_FILE, 'a')
				except OSError as exc:
					print "ERROR! Something is terribly wrong with opening file for append: "+PROJECTS_WITH_CHANGES_FILE
					print "Error: ",exc
					return -500
				proj_ptr.write(repo+"\n");
				proj_ptr.close()
				try:
					changes_ptr = open(OUT_DIR+repo+"_changes.txt", 'w')
				except OSError as exc:
					print "ERROR! Something is terribly wrong with opening _changes file for writing: "+OUT_DIR+repo+"_changes.txt"
					print "Error: ",exc
					return -500
				changes_ptr.write(output);
				changes_ptr.close()
				if (VERBOSE==1): print " [ COMMIT(S) FOUND ]"
			else:
				if (VERBOSE==1): print "no commits."
				elif (VERBOSE>=1): print "\tINFO: No commits made to '"+repo+"' repository."

	if (VERBOSE>=4): print "Returning from function "+sys_api._getframe().f_code.co_name+"()"
	return is_error


#Tue22Oct2013 11:46:12AM root@[~/LittleThings/ota_packager_1644/August_egg_git_repo/the_egg-tethering]# ls ~/egg_git_repo/the_egg-platform/
#SOURCES/  SRPM/
#
#Tue22Oct2013 12:28:21PM root@[~/LittleThings/ota_packager_1644/August_egg_git_repo/the_egg-tethering]# ls ~/egg_git_repo/the_egg-platform/*
#/root/egg_git_repo/the_egg-platform/SOURCES:
#aiccu_20070115.tar.gz	       ecg-auth-1.0.0.tar.gz	libdb-5.3.21.tar.gz*		      libnfnetlink-1.0.1.tar.bz2     libx264-1.0.0.tar.gz     php-5.4.13.tar.gz			       ZendGdata-1.12.3.tar.gz
#avpkt*			       egg-init-0.14.0.tar.gz	libmcrypt-2.5.7.tar.gz		      libnl-1.1.patched.tar.gz	     linux-atm-2.5.1.tar.gz*  phpliteadmin-1.9.3.tar.bz2
#cbq-0000.example*	       ffmpeg-1.0.0.tar.gz	libmnl-1.0.3.tar.bz2		      libnl-3.2.14.tar.gz	     ntp-4.2.4p2.tar.gz*      powerstat-0.1.tar.gz
#conntrackd.conf		       filter-requires-ntp.sh*	libnetfilter_conntrack-1.0.2.tar.bz2  libnl-doc-3.2.14.tar.gz	     ntp.conf*		      system-plugin-ia-generic-0.0.10.tar.bz2
#
#/root/egg_git_repo/the_egg-platform/SRPM:
#alarmaiccu-0.1-1.src.rpm		   crash-worker-sdk-0.1.3-6.1.src.rpm	 gphotofs-0.5-2.1.1.src.rpm	    libxml2-2.9.1-1.src.rpm	       org.tizen.bt-syspopup-0.2.56-2.6.src.rpm
#avahi-0.6.30-19.1.src.rpm		   crda-1.1.3_2013.02.13-4.src.rpm	 libgphoto2-2.5.2-2.1.src.rpm	    m2crypto-0.21.1-9.src.rpm	       org.tizen.pwlock-0.4.0-12.1.src.rpm
#bluetooth-share-0.0.46-2.15.src.rpm	   default-files-slp-0.0.2-86.1.src.rpm  libid3tag-0.15.1b-11.fc15.src.rpm  mhddfs-0.1.38-12.src.rpm	       starter-0.4.56-1.1.src.rpm
#bluez-4.101_19-19.1.src.rpm		   gd-2.0.35-18.src.rpm			 libmtp-1.1.6-0.src.rpm		    msmtp-1.4.29-1.src.rpm	       vo-aacenc-0.1.2-1.1.src.rpm
#
#[~/egg_git_repo/the_egg-platform]# git log --name-status --pretty=oneline tag4..tag3
#	0645aebdc45aa6c86d508aff744769fd75153b54 EGGPROTO-1570: UI Look and feel fix
#	M       SRPM/crash-worker-sdk-0.1.3-6.1.src.rpm
#	5948968ec991dae57d0ff2233f94ab12f9791a29 JIRA 1484 Tranfer egg metrics to chicken added a new cron job
#	M       SOURCES/egg-init-0.14.0.tar.gz
#	a571214b7d45554fd12b4123592cff16eb2e5038 the_egg-platform/system-pligin: Move dnsmasq and hostapd conf files to /data partition. Note: system-plugin-ia-generic creates dnsmasq.conf and hostapd.conf in /etc path. KS file 
#	M       SOURCES/system-plugin-ia-generic-0.0.10.tar.bz2
#	fa1a4538288381ff671a9da6a19f2495c38899ce Update Egg aiccu to use aiccu.conf to establish communication to server, so we can easly tonnect to DEV or PRODUCTION
#	M       SOURCES/aiccu_20070115.tar.gz
#	e8ad7ea2937c6f2e0a85d340f8ff3c5410367a07 EGGPROTO: 1296: pushing the crash log to server
#	A       SRPM/crash-worker-sdk-0.1.3-6.1.src.rpm
#	09070ed448aee91055b0e91f1fd59f912d24198a Jira 1523 , fixing the menu screen issue       modified:   SOURCES/system-plugin-ia-generic-0.0.10.tar.bz2     new file:   SRPM/starter-0.4.56-1.1.src.rpm
#	M       SOURCES/system-plugin-ia-generic-0.0.10.tar.bz2
#	A       SRPM/starter-0.4.56-1.1.src.rpm
#
def ishex(s): 
	return all(c in string.hexdigits for c in s)

#Utility function to strip text from end
def strip_end(text, suffix):
	if not text.endswith(suffix): return text
	return text[:len(text)-len(suffix)]


# This function:
#	- Takes spec_filename as input.
#	- Gives rpm_list as output.
#	- Opens the spec file, locates known tags (global variables) in the spec file, looks for "^%package" line
#	- Constructs RPM package name for each "^%package" line found and adds to rpm_list
def get_rpm_names(spec_file, rpm_list):
	if (VERBOSE>=4): print "Entering function "+sys_api._getframe().f_code.co_name+"()"

	try:
		fptr = open(spec_file, 'r')
	except OSError as exc:
		print "ERROR! Something is terribly wrong with opening file for read: "+spec_file
		print "Error: ",exc
		return -500
	if (VERBOSE>=3): print "\n=======================Parsing: '"+spec_file+"'==============================="
	pkg_name=""
	pkg_version=""
	pkg_release=""
	variant=""
	upstream_version=""
	majorver=""
	minorver=""
	tinyver=""
	crda_version=""
	regdb_version=""
	files_section_without_n_found = 0 # if "%files" section (without '-n') is missing, then default RPM is not created
	for line in fptr:
		if (VERBOSE>=4): print "==>Processing line: "+line,
		if (reg_ex.match("^Name[ \t]*", line)):
			if (VERBOSE>=3): print "\tINFO: Name: "+line,
			line = reg_ex.sub("\s*", "", line) # Remove all whitespace from 'line'
			pkg_name = line.split(":")[1] # Split "Name:Value", take 2nd param
			if (VERBOSE>=3): print "\tINFO: Parsed Name: "+pkg_name
			if ("%{variant}" in pkg_name): # valid only for kernel [Name: kernel-%{variant}]
				pkg_name = reg_ex.sub("%{variant}", variant, pkg_name) # Replace %{variant}
				if (VERBOSE>=3): print "\t\tINFO: Parsed Replaced Name: "+pkg_name
		if (reg_ex.match("^Version[ \t]*:", line)):
			if (VERBOSE>=3): print "\tINFO: Version: "+line,
			line = reg_ex.sub("\s*", "", line) # Remove all whitespace from 'line'
			pkg_version = "-"+line.split(":")[1] # Split "Name:Value", take 2nd param
			if (VERBOSE>=3): print "\tINFO: Parsed Version: "+pkg_version
			if ("%{upstream_version}" in pkg_version): # valid only for kernel [Version: -%{upstream_version}]
				pkg_version = reg_ex.sub("%{upstream_version}", upstream_version, pkg_version) # Replace %{upstream_version}
				if (VERBOSE>=3): print "\t\tINFO: Parsed Replaced(upstream_version) Version: "+pkg_version
			#Version:		%{majorver}.%{minorver}.%{tinyver}
			if ("%{majorver}" in pkg_version): # valid only for libvpx
				pkg_version = reg_ex.sub("%{majorver}", majorver, pkg_version) # Replace %{majorver}
				if (VERBOSE>=3): print "\t\tINFO: Parsed Replaced(majorver) Version: "+pkg_version
			if ("%{minorver}" in pkg_version): # valid only for libvpx [Version:%{majorver}.%{minorver}.%{tinyver}]
				pkg_version = reg_ex.sub("%{minorver}", minorver, pkg_version) # Replace %{minorver}
				if (VERBOSE>=3): print "\t\tINFO: Parsed Replaced(minorver) Version: "+pkg_version
			if ("%{tinyver}" in pkg_version): # valid only for libvpx [Version:%{majorver}.%{minorver}.%{tinyver}]
				pkg_version = reg_ex.sub("%{tinyver}", tinyver, pkg_version) # Replace %{tinyver}
				if (VERBOSE>=3): print "\t\tINFO: Parsed Replaced(tinyver) Version: "+pkg_version
			#Version:        %{crda_version}_%{regdb_version}
			if ("%{crda_version}" in pkg_version): # valid only for crda
				pkg_version = reg_ex.sub("%{crda_version}", crda_version, pkg_version) # Replace %{crda_version}
				if (VERBOSE>=3): print "\t\tINFO: Parsed Replaced(crda_version) Version: "+pkg_version
			if ("%{regdb_version}" in pkg_version): # valid only for crda
				pkg_version = reg_ex.sub("%{regdb_version}", regdb_version, pkg_version) # Replace %{regdb_version}
				if (VERBOSE>=3): print "\t\tINFO: Parsed Replaced(regdb_version) Version: "+pkg_version

		if (reg_ex.match("^Release[ \t]*:", line)):
			if (VERBOSE>=3): print "\tINFO: Release: "+line,
			line = reg_ex.sub("\s*", "", line) # Remove all whitespace from 'line'
			rel = line.split(":")[1] # Split "Name:Value", take 2nd param
			if ("%" in rel): #Handle case = Release:2%{?dist}
				if (VERBOSE>=3): print "\tINFO: Removing % from Version: "+rel
				rel = rel.split("%")[0]
			pkg_release = "-"+rel
			if (VERBOSE>=3): print "\tINFO: Parsed Version: "+pkg_release
		if (reg_ex.match("^%global[ \t]*", line)):
			if (VERBOSE>=3): print "\tINFO: global: "+line,
			lst = line.split() #We are checking for 'majorver', 'minorver' & 'tinyver'
			if (lst[1] == "majorver"):
				majorver = lst[2]
				if (VERBOSE>=3): print "\tINFO: Parsed majorver: "+majorver
			elif (lst[1] == "minorver"):
				minorver = lst[2]
				if (VERBOSE>=3): print "\tINFO: Parsed minorver: "+minorver
			elif (lst[1] == "tinyver"):
				tinyver = lst[2]
				if (VERBOSE>=3): print "\tINFO: Parsed tinyver: "+tinyver


		if (reg_ex.match("^%define[ \t]*", line)):
			if (VERBOSE>=3): print "\tINFO: define: "+line,
			lst = line.split() #We are checking for 'upstream_version' & 'variant'
			if (lst[1] == "variant"):
				variant = lst[2]
				if (VERBOSE>=3): print "\tINFO: Parsed variant: "+variant
			elif (lst[1] == "upstream_version"):
				upstream_version = lst[2]
				if (VERBOSE>=3): print "\tINFO: Parsed upstream_version: "+upstream_version
			elif (lst[1] == "crda_version"):
				crda_version = lst[2]
				if (VERBOSE>=3): print "\tINFO: Parsed crda_version: "+crda_version
			elif (lst[1] == "regdb_version"):
				regdb_version = lst[2]
				if (VERBOSE>=3): print "\tINFO: Parsed regdb_version: "+regdb_version
		if (reg_ex.match("^%package[ \t]", line)):
			if (VERBOSE>=3): print "\tINFO: PACKAGE: "+line,
			line = reg_ex.sub("%{name}", pkg_name, line) # Replace %{name}
			if (VERBOSE>=4): print "\tINFO: PACKAGE(after %{name} check): "+line,
			lst = line.split()
			new_pkg=""
			sub_pkg=""
			if (lst[1] == "-n"):
				new_pkg = lst[2]+pkg_version+pkg_release+".i586.rpm"
				if (VERBOSE>=2): print "INFO: New-package: "+new_pkg
				rpm_list.append(new_pkg)
			else:
				sub_pkg = pkg_name+"-"+lst[1]+pkg_version+pkg_release+".i586.rpm"
				if (VERBOSE>=2): print "INFO: sub-package: "+sub_pkg
				rpm_list.append(sub_pkg)
		if (line.rstrip() == "%files"): #strip any trailing whitespace
			files_section_without_n_found = 1
		if (reg_ex.match("^%changelog\s", line)): #"\s" matches any whitespace character
			if (VERBOSE>=2): print "\tINFO: Changlog section begins. Parsing Stopped."
			break

	fptr.close()
	if (pkg_name != "") and (pkg_version != "") and (pkg_release != ""): #Package name & release info found in spec file
		#RPM naming convention: name[-subpkg]-version-release.architecture.rpm
		rpm = pkg_name+pkg_version+pkg_release+".i586.rpm"
		if (files_section_without_n_found):
			if (VERBOSE>=2): print "INFO: Default-package: "+rpm
			rpm_list.insert(0, rpm)
		else:
			if (VERBOSE>=2): print "INFO: Spec file ["+spec_file+"] does not create default-package: "+rpm
	else:
		print "\nERROR: Spec file '"+spec_file+"' does not have mandatory Name/Version/Release parameter!"
		print "Name='"+pkg_name+"' "+"Version='"+pkg_version+"' "+"Release='"+pkg_release+"' "
		return -1

	if (VERBOSE>=4): print "Returning from function "+sys_api._getframe().f_code.co_name+"()"
	return 0



# This function:
#	- opens file - the_egg-platform_changes.txt and parses the "git log" output.
#	- If the log shows that any SOURCES or SPEC file is removed, stores filename in different lists (to skip remaining changes done on this file)
#	- If there has been any other git operation (modify,add, etc), the filename is stored in srpm_list & sources_list
#	- Duplicate entries are removed from srpm_list & sources_list
#	- handle_source_repo(srpm_list + sources_list) is invoked to get the names RPMs that are generated by the spec files that correspond to the srpm/source projects.
def handle_repo_platform(repo, wfptr):
	try:
		changes_ptr = open(OUT_DIR+repo+"_changes.txt", 'r')
	except OSError as exc:
		print "ERROR! Something is terribly wrong with opening file for reading: "+OUT_DIR+repo+"_changes.txt"
		print "Error: ",exc
		return -500
	if (VERBOSE >= 1): print "INFO: Processing commits in 'the_egg-platform' repository..."
	git_log_as_list = list(changes_ptr)
	changes_ptr.close()
	git_log_as_list = [x.strip() for x in git_log_as_list] # Remove newline at end of each list entry
	git_log_as_list = [x for x in git_log_as_list if x] # Remove empty list entries
	#Check list git_log_as_list for known entries 
	# 1 -> "[A-Z]\tSRPM/"
	# 2 -> "[A-Z]\tSOURCES/"
	# 3 -> [A-Za-z0-9]*40 (reg ex)
	srpm_list = []
	sources_list = []
	deleted_srpm_list = []
	deleted_sources_list = []
	for line in git_log_as_list:
		if (VERBOSE >= 3): print "INFO: Processing line(platform): "+line
		str_deletesrpm_regex="^D\tSRPM/"
		str_deletesources_regex="^D\tSOURCES/"
		str_srpm_regex="^[A-Z]\tSRPM/"
		str_sources_regex="^[A-Z]\tSOURCES/"
		expr_srpm_regex = reg_ex.compile(str_srpm_regex);
		expr_sources_regex = reg_ex.compile(str_sources_regex);
		expr_deletesrpm_regex = reg_ex.compile(str_deletesrpm_regex);
		expr_deletesources_regex = reg_ex.compile(str_deletesources_regex);
		if (reg_ex.match(expr_deletesrpm_regex, line)): # Found a deleted SRPM
			srpm = line[7:]
			if (VERBOSE >= 2): print "\tINFO: Found deleted SRPM: "+srpm
			deleted_srpm_list.append(srpm)
		elif (reg_ex.match(str_deletesources_regex, line)): # Found a deleted SOURCES file
			source = line[10:]
			if (VERBOSE >= 2): print "\tINFO: Found deleted SOURCES: "+source
			deleted_sources_list.append(source)
		elif (reg_ex.match(expr_srpm_regex, line)): # Found a modified SRPM
			srpm = line[7:]
			if (VERBOSE >= 2): print "\tINFO: Found change in an SRPM: "+srpm
			if (srpm in deleted_srpm_list):
				if (VERBOSE >= 2): print "INFO: SRPM File '"+srpm+"' was deleted by a recent commit. Skipping old changes..."
			else: srpm_list.append(srpm)
		elif (reg_ex.match(expr_sources_regex, line)): # Found a modified SOURCES
			source = line[10:]
			if (VERBOSE >= 2): print "\tINFO: Found change in a Source: "+source
			if (source in deleted_srpm_list):
				if (VERBOSE >= 2): print "INFO: SOURCE File '"+source+"' was deleted by a recent commit. Skipping old changes..."
			else: sources_list.append(source)
		else:
			word=line.split()[0] #Check if line begins with 40byte git-hash
			if ((40==len(word)) and (ishex(word))):
				if (VERBOSE >= 3): print "\tINFO: (platform) Line appears to be a git-hash. Ignoring..."
			elif (line.endswith(".ks")): #Change to kickstart file detected
				if (VERBOSE >= 1): print "\tWARNING: Kickstarter changes detected, file:"+line
				if (VERBOSE >= 1): print "Incremental OTA upgrade may not suffice. Please check if Full OTA upgrade is required!"
			else:
				if (VERBOSE >= 1): print "\tWARNING: Ignoring modifications to file:"+line

	if ((len(srpm_list) > 0) and (VERBOSE >= 3)): print "\tINFO: Following src.rpm's changed:",srpm_list
	srpm_list = list(set(srpm_list))
	if ((len(srpm_list) > 0) and (VERBOSE >= 2)): print "\tINFO: src.rpm's (after removing duplicates):",srpm_list

	if ((len(sources_list) > 0) and (VERBOSE >= 3)): print "\tINFO: Following zipped source changed:",sources_list
	sources_list = list(set(sources_list))
	if ((len(sources_list) > 0) and (VERBOSE >= 2)): print "\tINFO: SOURCES (after removing duplicates):",sources_list

	# Unify srpm_list & sources_list. Trim name to extract project folder name.
	# Go into each folder in /root/rpmbuild/BUILD/ (folder for srpm/source.tar.gz)
	# Get names of SPEC files from respective "packaging/" folder
	tmp_list = srpm_list + sources_list
	platform_list = []
	if (len(tmp_list) > 0):
		if (VERBOSE>=2): print "\tINFO: Following platform files(srpm+sources) changed:",tmp_list
		for proj in tmp_list:
			p = proj
			if (VERBOSE>=3): print "\t\tProcessing: "+p
			p = reg_ex.sub(r"-[0-9].*", "", p) # truncate filename
			if (VERBOSE>=3): print "\t\tAfter truncating version: "+p
			# truncate filename (*.src.rpm OR *.tar.gz) (Useful when version info is not present in filename)
			p = reg_ex.sub(r"[.]src[.].*|[.]tar[.].*", "", p)
			if (VERBOSE>=3): print "\t\tAfter truncating DOT: "+p
			platform_list.append(p)
		if (VERBOSE>=1): print "\tINFO: platform files(srpm+sources) after processing:",platform_list

		for bld_dir in platform_list:
			if bld_dir in EXPLICIT_IGNORE_PLATFORM_PROJECT:
				print "WARNING: platform repo: project '"+bld_dir+"' has code changes, but is being excluded by user..."
				continue
			if (VERBOSE>=2): print "platform: identifying RPMs in project: "+bld_dir
			#Save resulting RPM to TMP_MNFST_FILE
			if (0 != handle_source_repo(bld_dir, wfptr, BUILD_BASE)):
				print "ERROR! handle_repo_platform() -> handle_source_repo() failed for project: "+bld_dir
				return -1
				#TODO avoid this return to ignore any errors that occur in handling any of the source(normal) repositories
	else:
		if (VERBOSE >= 2): print "\nWARNING: No OTA contributing commits made to platform repository."

	if (VERBOSE>=4): print "Completed handling 'platform' repository\n"
	return 0



# This function:
#	- opens file - the_egg-tools_changes.txt and parses the "git log" output.
#	- If the log shows that any SPEC/* file is removed, stores filename in deleted_spec_list
#	- If there has been any other git operation (modify,add, etc), the filename is stored in spec_file_list
#	- Duplicate entries are removed from spec_file_list
#	- get_rpm_names(spec_file_list) is invoked to get the names RPMs that are generated by the spec files.
def handle_repo_tools(repo, wfptr):
	try:
		changes_ptr = open(OUT_DIR+repo+"_changes.txt", 'r')
	except OSError as exc:
		print "ERROR! Something is terribly wrong with opening file for reading: "+OUT_DIR+repo+"_changes.txt"
		print "Error: ",exc
		return -500

	if (VERBOSE >= 1): print "INFO: Processing commits in 'the_egg-tools' repository..."
	wfptr.write("#########################################\n")
	wfptr.write("##    "+repo+" changes...\n")
	git_log_as_list = list(changes_ptr)
	changes_ptr.close()
	git_log_as_list = [x.strip() for x in git_log_as_list] # Remove newline at end of each list entry
	git_log_as_list = [x for x in git_log_as_list if x] # Remove empty list entries

	#Check list git_log_as_list for known entries 
	# 1 -> "D\tSPECS/"
	# 2 -> "[A-Z]\tSPECS/"
	# 3 -> [A-Za-z0-9]*40 (reg ex)
	spec_file_list = []
	deleted_spec_list = []	# If a spec file was deleted more recently, ignore any more commits made to it previously
	for line in git_log_as_list:
		if (VERBOSE >= 3): print "\tINFO: Processing line(tools): "+line
		str_deletedspec_regex="^D\tSPECS/"
		str_spec_regex="^[A-Z]\tSPECS/"
		expr_deletedspec_regex = reg_ex.compile(str_deletedspec_regex);
		expr_spec_regex = reg_ex.compile(str_spec_regex);
		if (reg_ex.match(str_deletedspec_regex, line)): # Found a deleted spec file. Don't do anything
			deleted_spec = line[2:]
			if (VERBOSE >= 2): print "\tINFO: spec is deleted. Igonring line: "+deleted_spec
			deleted_spec_list.append(deleted_spec)
		elif (reg_ex.match(expr_spec_regex, line)): # Found a modified file under SPEC/ 
			spec = line[2:]
			if (VERBOSE >= 2): print "\tINFO: Found change in tools/SPECS: "+spec
			if (spec in deleted_spec_list):
				if (VERBOSE >= 2): print "\tINFO: The file ('"+spec+"') was deleted by more recent commit. Ignoring old changes..."
			else:
				if os_api.path.basename(spec) in EXPLICIT_IGNORE_SPEC_FILE:
					print "WARNING: tools: Spec file '"+spec+"' has valid changes, but is being excluded by user..."
					continue
				spec_file_list.append(spec)
		else:
			word=line.split()[0] #Check if line begins with 40byte git-hash
			if ((40==len(word)) and (ishex(word))):
				if (VERBOSE >= 3): print "\tINFO: (tools) Line appears to be a git-hash. Ignoring..."
			else:
				if (VERBOSE >= 1): print "\tWARNING: Ignoring unimportant modifed file(in tools repo):"+line

	if (len(spec_file_list) > 0):
		if (VERBOSE>=1): print "\tINFO: Following spec files changed in 'the_egg-tools' repository:",spec_file_list
		spec_file_list = list(set(spec_file_list))	#Remove duplicate entries
		for spec_file in spec_file_list:
			rpm_list=[]
			sfile = EGG_GIT_REPO+repo+"/"+spec_file
			if (0 != get_rpm_names(sfile, rpm_list)):
				print "ERROR: Failed to get RPM package name from spec file: "+sfile
				return -401
			else:
				if (len(rpm_list) > 0):
					if (VERBOSE >= 2): print "\tINFO: '"+sfile+"' provides: ",rpm_list
					for rpm in rpm_list:
						wfptr.write(rpm+"\n")
				else:
					print "ERROR: '"+sfile+"' does not provide any RPMs!"
					return -502

	else:
		if (VERBOSE >= 1): print "\tWARNING: Commits found in '"+repo+"'. But not to spec files."

	if (VERBOSE>=4): print "Completed handling 'the_egg-tools' repository\n"
	return 0



# This function:
#	- opens file - the_egg-third-party_changes.txt and parses the "git log" output.
#	- If the log shows that any bin_rpms/* file is removed, stores filename in deleted_rpm_list
#	- If there has been any other git operation (modify,add, etc), the filename is stored in bin_rpm_list
#	- Duplicate entries are removed from bin_rpm_list
#	- Modified bin rpm filenames are written to wfptr (for further processing)
def handle_repo_thirdparty(repo, wfptr):
	try:
		changes_ptr = open(OUT_DIR+repo+"_changes.txt", 'r')
	except OSError as exc:
		print "ERROR! Something is terribly wrong with opening file for reading: "+OUT_DIR+repo+"_changes.txt"
		print "Error: ",exc
		return -500

	if (VERBOSE >= 1): print "INFO: Processing commits in 'the_egg-third-party' repository..."
	wfptr.write("#########################################\n")
	wfptr.write("##    "+repo+" changes...\n")
	git_log_as_list = list(changes_ptr)
	changes_ptr.close()
	git_log_as_list = [x.strip() for x in git_log_as_list] # Remove newline at end of each list entry
	git_log_as_list = [x for x in git_log_as_list if x] # Remove empty list entries
	#Check list git_log_as_list for known entries 
	# 1 -> "[A-Z]\tbin_rpms/"
	# 2 -> [A-Za-z0-9]*40 (reg ex)
	bin_rpm_list = []	#RPMs added/modified/renamed
	deleted_rpm_list=[]	# If an RPM was deleted more recently, ignore any more commits made to it previously
	for line in git_log_as_list:
		if (VERBOSE >= 3): print "\tINFO: Processing line(thirdparty): "+line
		str_deletedrpm_regex="^D\tbin_rpms/"
		str_binrpm_regex="^[A-Z]\tbin_rpms/"
		expr_deletedrpm_regex = reg_ex.compile(str_deletedrpm_regex);
		expr_binrpm_regex = reg_ex.compile(str_binrpm_regex);
		if (reg_ex.match(str_deletedrpm_regex, line)): # Found a deleted bin RPM. Don't do anything
			deleted_rpm = line[11:]
			if (VERBOSE >= 2): print "\tINFO: bin rpm is deleted. Igonring line: "+deleted_rpm
			deleted_rpm_list.append(deleted_rpm)
		elif (reg_ex.match(expr_binrpm_regex, line)): # Found a modified/added binrpm
			binrpm = line[11:]
			if (VERBOSE >= 2): print "\tINFO: Found change in a bin_rpm: "+binrpm
			if (binrpm in deleted_rpm_list):
				if (VERBOSE >= 2): print "\tINFO: This rpm ('"+binrpm+"') was deleted by more recent commit. Ignoring old changes..."
			else:
				if (binrpm not in bin_rpm_list): #add to list if it is not already present
					bin_rpm_list.append(binrpm)
					wfptr.write(binrpm+"\n")
		else:
			word=line.split()[0] #Check if line begins with 40byte git-hash
			if ((40==len(word)) and (ishex(word))):
				if (VERBOSE >= 3): print "\tINFO: (thirdparty) Line appears to be a git-hash. Ignoring..."
			else:
				print "\tWARNING: UNHANDLED log message seen in file:"+line

	if (len(bin_rpm_list) > 0):
		if (VERBOSE>=1): print "\tINFO: Following bin_rpm's changed:",bin_rpm_list

	if (VERBOSE>=4): print "Completed handling 'the_egg-third-party' repository\n"
	return 0



# This function:
#	- Checks the size of file - the_egg-preos_changes.txt
#	- If the file size is > 1 bytes, stitch.preos.bin has to be copied.
#	- If size is less, dont-do-anything.
def handle_repo_preos(repo, wfptr):
	global HYBRID_PARAMS
	git_log_file = OUT_DIR+repo+"_changes.txt"
	size_log_file = 0

	if (VERBOSE >= 1): print "INFO: Processing commits in 'the_egg-preos' repository..."
	try:
		size_log_file = os_api.path.getsize(git_log_file)
		if (VERBOSE>=3): print "Size of '"+git_log_file+"' is:",size_log_file
	except OSError as exc:
		print "ERROR! Unable to check size of file: "+git_log_file
		return -500

	if (size_log_file > 1): # extract stitch.preos.bin from LifePlatform...zip file
		if (not os_api.access(FULLBUILD_ZIPFILE, os_api.R_OK)):
			print "ERROR! Full build not found: "+FULLBUILD_ZIPFILE
			return -404
		if (VERBOSE >= 1): print "INFO: Special project 'the_egg-preos' has changes. Adding 'stitch.preos.bin'"
		HYBRID_PARAMS = HYBRID_PARAMS+"=stitch.preos.bin"

		os_api.chdir(OUT_DIR)
		preos_bin_file = "stitch.preos.bin"
		#extract 'stitch.preos.bin' from FullBuild
		CMD = "unzip " + FULLBUILD_ZIPFILE + " " + preos_bin_file
		process=subp_api.Popen(CMD, stdout=subp_api.PIPE, stderr=subp_api.PIPE, shell=True)
		output,error=process.communicate()
		if (VERBOSE>=2): print "Command: "+CMD
		if (VERBOSE>=3): print "\tstdout: '"+output+"'"
		if (VERBOSE>=3): print "\tstderr: '"+error+"'"
		if (error):
			print "ERROR: failed to unzip 'stitch.preos.bin' from: "+FULLBUILD_ZIPFILE
			print "\tError string: "+error
			print "\tOuput string: "+output
			return -500

		wfptr.write("#########################################\n")
		wfptr.write("##    "+repo+" changes...\n")
		wfptr.write(OUT_DIR+"stitch.preos.bin\n")
	else:
		if (VERBOSE >= 2): print "INFO: Special project 'the_egg-preos' has no changes."


	if (VERBOSE>=4): print "Completed handling 'the_egg-preos' repository\n"
	return 0



# This function:
#	- Extracts 'stitch.normalos.bin' from FULLBUILD_ZIPFILE and adds its filename to wfptr
# Kernel is semi-special project
def handle_repo_kernel(repo, wfptr):
	global HYBRID_PARAMS
	if (VERBOSE >= 1): print "INFO: Special project 'the_egg-kernel' has changes. Adding 'stitch.normalos.bin'"
	HYBRID_PARAMS = HYBRID_PARAMS+"=stitch.normalos.bin"
	os_api.chdir(OUT_DIR)
	kern_bin_file = "stitch.normalos.bin"
	#extract 'stitch.normalos.bin' from FullBuild
	CMD = "unzip " + FULLBUILD_ZIPFILE + " " + kern_bin_file
	process=subp_api.Popen(CMD, stdout=subp_api.PIPE, stderr=subp_api.PIPE, shell=True)
	output,error=process.communicate()
	if (VERBOSE>=2): print "Command: "+CMD
	if (VERBOSE>=3): print "\tstdout: '"+output+"'"
	if (VERBOSE>=3): print "\tstderr: '"+error+"'"
	if (error):
		print "ERROR: failed to unzip '"+kern_bin_file+"' from: "+FULLBUILD_ZIPFILE
		print "\tError string: "+error
		print "\tOuput string: "+output
		return -500

	wfptr.write(OUT_DIR+kern_bin_file+"\n")
	return 0



# This function:
#	- identifies RPMs generated by a project which is stored in git repository as complete source code.
#	- checks if repo/packaging/FILE.spec is present or not.
#	- invokes "get_rpm_names()" to get the names of RPMs generated by that spec file
def handle_source_repo(repo, wfptr, base_path):
	wfptr.write("#########################################\n")
	wfptr.write("##    "+repo+" changes...\n")
	path=base_path+repo+"/packaging/"
	if (not os_api.path.isdir(path)):
		print "ERROR: Packaging directory not found: "+path
		print "Follow RPM guidelines and create packaging/ directory."
		return -400
	os_api.chdir(path)
	spec_file_list=[]
	if (VERBOSE>=1): print "INFO: Source repo '"+repo+"' has commits. Finding .spec file in: "+path
	for root, dirs, files in os_api.walk("."):
		for file in files:
			if file.endswith(".spec"):
				f = os_api.path.join(root, file)
				if (VERBOSE>=2): print "Found file: "+f
				spec_file_list.append(f)
		break
	count = len(spec_file_list)
	if (count == 0):
		print "ERROR: Spec file not found in: "+path
		print "Follow RPM guidelines and have spec file in packaging/ directory."
		return -400
	elif (count > 1):
		print "ERROR: More than one Spec file found in: "+path
		print "Files: ",spec_file_list
		print "Follow RPM guidelines and have just one spec file in packaging/ directory."
		return -401

	if (VERBOSE>=3): print "\tINFO: Parsing '"+spec_file_list[0]+"' for RPMs..."
	rpm_list=[]
	if (0 != get_rpm_names(spec_file_list[0], rpm_list)):
		print "ERROR: Failed to get RPM package name from spec file: "+spec_file_list[0]
		return -401
	else:
		if (len(rpm_list) > 0):
			if (VERBOSE>=1): print "\tINFO: '"+spec_file_list[0]+"' provides: ",rpm_list
			for rpm in rpm_list:
				wfptr.write(rpm+"\n")
			###STITCH..BIN handling: Begin
			if ("the_egg-kernel" == repo):
				if (0 != handle_repo_kernel(repo, wfptr)):
					print "ERROR: handle_repo_kernel() failed!"
					return -400
			###STITCH..BIN handling: End
		else:
			print "ERROR: '"+spec_file_list[0]+"' does not provide any RPMs!"
			return -502

	return 0


# This function:
#	- takes tmp_file pointer as input. This contains names of all RPM files that need to be part of OTA update
#	- the tmp_file lists a source package and all the RPMs that it provides for the OTA
#	- Read from tmp_file, remove duplicates
#	- copy RPM files from /root/GBS-ROOT/local/ /RPMS/ into update/ folder
#	- ZIPs the contents of the folder, calculates the size of zip file
#	- Identifies dependencies between RPMs needed for OTA and sorts them
#	- Opens "MNFST_FILE" and writes:
#		- (1) number of RPM files for OTA
#		- (2) Size of update.zip in KB
#		- (3) Build version
#		- (n) Names of RPM files
def prepare_ota_package(tmp_file):
	if os_api.access(tmp_file, os_api.R_OK):
		try:
			tmp_ptr = open(tmp_file, 'r')
		except OSError as exc:
			print "ERROR! Something is terribly wrong with opening file for reading: "+tmp_file
			print "Error: ",exc
			return -500
		if (VERBOSE>=2): print "Preparing manifest file: "+MNFST_FILE
		rpms4ota = list(tmp_ptr)
		tmp_ptr.close()
		rpms4ota = [x.strip() for x in rpms4ota] # Remove newline at end of each list entry
		rpms4ota = [x for x in rpms4ota if (x and not x.startswith("#"))] # Remove empty & comment entries
		if (len(rpms4ota) > 0):
			if (VERBOSE>=2): print "\n********************************************************"
			if (VERBOSE>=2): print "INFO: RPMs (before removing duplicates):",rpms4ota
			rpms4ota = list(set(rpms4ota)) # Remove duplicates
			if (VERBOSE>=2): print "\nINFO: RPMs (after removing duplicates):",rpms4ota
			rpms4ota = [item for item in rpms4ota if item not in EXPLICIT_REMOVE_PACKAGES_FOR_OTA] # Remove user specified RPMs
			if (VERBOSE>=2): print "\nINFO: RPMs (after removing user configured rpms):",rpms4ota
			rpms4ota.extend(EXPLICIT_ADD_PACKAGES_FOR_OTA) #Add user specified RPMs
			if (VERBOSE>=2): print "\nINFO: RPMs for OTA (after adding user configured rpms):",rpms4ota
			if (VERBOSE==1): print "\nINFO: Final set of RPMs for OTA update:",rpms4ota

			zip_filename = OTA_ZIP_FILENAME
			#Copy all 'rpms4ota' to OUT_DIR+"/update"
			os_api.chdir(RPM_DIR)
			src = ' '.join(rpms4ota)
			dst = OUT_DIR+zip_filename[:-4]+"/"
			os_api.mkdir(dst)
			if (VERBOSE>=1): print "INFO: Copying '"+str(len(rpms4ota))+"' files from '"+RPM_DIR+"' to "+dst
			CMD="cp  "+ src + "   " + dst
			process=subp_api.Popen(CMD, stdout=subp_api.PIPE, stderr=subp_api.PIPE, shell=True)
			output,error=process.communicate()
			if (VERBOSE>=2): print "Command: "+CMD
			if (VERBOSE>=3): print "\tstdout: '"+output+"'"
			if (VERBOSE>=3): print "\tstderr: '"+error+"'"
			if ((error) or ("zip warning:" in output)):
				print "ERROR: failed to copy RPM files to: "+dst
				print "\tError string: "+error
				print "\tOuput string: "+output
				return -500
			if (VERBOSE>=2): print "\tRPM files copied to: "+dst


			#Make zip file of all RPMs: rpms4ota
			if (VERBOSE>=2): print "Preparing to zip the OTA contents..."
			os_api.chdir(OUT_DIR)
			CMD="zip --junk-paths "+zip_filename+" "+zip_filename[:-4]+"/*"
			if (VERBOSE>=2): print "\tINFO: Preparing: "+zip_filename
			process=subp_api.Popen(CMD, stdout=subp_api.PIPE, stderr=subp_api.PIPE, shell=True)
			output,error=process.communicate()
			if (VERBOSE>=2): print "Command: "+CMD
			if (VERBOSE>=3): print "\tstdout: '"+output+"'"
			if (VERBOSE>=3): print "\tstderr: '"+error+"'"
			if ((error) or ("zip warning:" in output)):
				print "ERROR: Failed to create zip file: "+zip_filename
				print "\tError string: "+error
				print "\tOuput string: "+output
				return -500
			if (VERBOSE>=2): print "\tOTA zip file is ready: " +zip_filename

			#Calculate ZIP file size
			CMD="ls -l "+zip_filename
			if (VERBOSE>=2): print "\tINFO: Calculating size of file: "+zip_filename
			process=subp_api.Popen(CMD, stdout=subp_api.PIPE, stderr=subp_api.PIPE, shell=True)
			output,error=process.communicate()
			if (VERBOSE>=2): print "Command: "+CMD
			if (VERBOSE>=3): print "\tstdout: '"+output+"'"
			if (VERBOSE>=3): print "\tstderr: '"+error+"'"
			if (error):
				print "ERROR! Finding size of '"+zip_filename+"' failed."
				print "Description: '"+error+"'"
				return -400
			ls_as_list = output.split()
			size = "{0:.0f}".format(int(ls_as_list[4])/(1024.0)) # Extract 5th field, convert to KB, round to integer

			#Create manifest file with all required info:
			try:
				mnfst_ptr = open(MNFST_FILE, 'w')
			except OSError as exc:
				print "ERROR! Something is terribly wrong with opening file for writing: "+MNFST_FILE
				print "Error: ",exc
				return -500
			#Get software build number from the LifePlatform filename passed as argv[3]
			tmp_name = os_api.path.basename(FULLBUILD_ZIPFILE) # Remove preceding path
			tmp_name = os_api.path.splitext(tmp_name)[0] # Remove extension (.zip)
			tmp_name = strip_end(tmp_name, "-eggdevel") # Remove branch name (-eggdevel)
			tmp_name = strip_end(tmp_name, "-master") # Remove branch name (-master), if any
			if (tmp_name == ""): BLD_NUM = "NA\n"
			else: BLD_NUM = tmp_name + "\n"
			'''
			if os_api.access(BUILD_ITERATION_NUMBER_FILE, os_api.R_OK):
				try:
					fptr_bldnum = open(BUILD_ITERATION_NUMBER_FILE, 'r')
				except OSError as exc:
					print "ERROR! Something is terribly wrong with opening file for reading: "+BUILD_ITERATION_NUMBER_FILE
					print "Error: ",exc
					return -500
				if (VERBOSE>=2): print "Reading Build Iteration info file: "+BUILD_ITERATION_NUMBER_FILE
				lines = list(fptr_bldnum)
				if (lines[1]!=""): BLD_NUM = lines[1] #2nd line should contain version info
				if (not BLD_NUM.endswith("\n")): BLD_NUM=BLD_NUM+"\n"
				fptr_bldnum.close()
			else:
				print "Warning: File not found: "+BUILD_ITERATION_NUMBER_FILE
				print "Setting build version information as: "+BLD_NUM,
			'''

			if (VERBOSE>=1): print "\tINFO: Size of '"+zip_filename+"' in Bytes="+(ls_as_list[4])+". Size in KB="+size+". Build number="+BLD_NUM,

			# Write to manifest: Total number of files in OTA
			#					 Total size of OTA.zip file (in KB)
			#					 v.1.0.0.BUILDDATE-BUILDITERATION
			mnfst_ptr.write(str(len(rpms4ota))+"\n"+str(size)+"\n"+BLD_NUM)
			###STITCH..BIN handling: Begin
			if ("" == HYBRID_PARAMS): mnfst_ptr.write("hybrid=false\n")
			else: mnfst_ptr.write("hybrid=true"+HYBRID_PARAMS+"\n")
			###STITCH..BIN handling: End

			reordered_list=[]
			if (0 != reorder_manifest(rpms4ota, reordered_list)):
				print "ERROR! Failed to reorder manifest."
				print "\tInput=",rpms4ota
				print "\tOutput=",reordered_list
				return -500
			for proj in reordered_list: #Names of RPMs for OTA
				mnfst_ptr.write(proj+"\n")

			## UNINSTALL_PACKAGES: Begin
			UNINSTALL_PACKAGES = [x for x in UNINSTALL_PACKAGES_THROUGH_OTA if x] #rem empty list entries
			if (UNINSTALL_PACKAGES): #if we have RPMs to be uninstalled
				if (VERBOSE>=1): print "INFO: Packages to UNINSTALL:", UNINSTALL_PACKAGES
				mnfst_ptr.write("UNINSTALL")
				for proj in UNINSTALL_PACKAGES:
					mnfst_ptr.write(","+proj)
				mnfst_ptr.write("\n")
			## UNINSTALL_PACKAGES: End
			mnfst_ptr.close()

			#Create path like: /root/OTA_UPDATE/OTA_Package_v.1.0.0-20131101-155553_v.1.0.0-20131121-154601
			ADDITIONAL_OUT_DIR=ADDITIONAL_OUT_DIR_BASE + OUT_DIR.replace(EGG_GIT_REPO, "")
			if (os_api.path.isdir(ADDITIONAL_OUT_DIR)):
				if os_api.access(ADDITIONAL_OUT_DIR+OTA_ZIP_FILENAME, os_api.R_OK):
					RM_CONTENTS = "rm -rf " + ADDITIONAL_OUT_DIR+OTA_ZIP_FILENAME
					if (VERBOSE>=1): print "Removing old zip: "+ADDITIONAL_OUT_DIR+OTA_ZIP_FILENAME
					if (0 != os_api.system(RM_CONTENTS)):
						print "ERROR: Failed to remove ota zip file from alternate-location!\n\t"+RM_CONTENTS
				if os_api.access(ADDITIONAL_OUT_DIR+OUTPUT_MANIFEST_FILE, os_api.R_OK):
					RM_CONTENTS = "rm -rf " + ADDITIONAL_OUT_DIR+OUTPUT_MANIFEST_FILE
					if (VERBOSE>=1): print "Removing old manifest: "+ADDITIONAL_OUT_DIR+OUTPUT_MANIFEST_FILE
					if (0 != os_api.system(RM_CONTENTS)):
						print "ERROR: Failed to remove ota manifest file from alternate-location!\n\t"+RM_CONTENTS
			else:
				if (VERBOSE>=1): print "Creating alternate location to store incremental OTA package: "+ADDITIONAL_OUT_DIR
				os_api.makedirs(ADDITIONAL_OUT_DIR)

			ADDL_CP_CMD = "cp " + OUT_DIR+OTA_ZIP_FILENAME + " " + MNFST_FILE+ " " +ADDITIONAL_OUT_DIR
			if (VERBOSE>=2): print "Copying to alternate location: "+ADDITIONAL_OUT_DIR+"\n\t"+ADDL_CP_CMD
			if (0 != os_api.system(ADDL_CP_CMD)):
						print "ERROR: Failed to remove ota manifest file from alternate-location!\n\t"+RM_CONTENTS
			txt="%s"%"#####"
			print txt.center(len(MNFST_FILE)+10, "=")
			print "OTA pkg : "+OUT_DIR+zip_filename
			print "Manifest: "+MNFST_FILE
			print "Alternate location: "+ADDITIONAL_OUT_DIR
			print txt.center(len(MNFST_FILE)+10, "=")

		else:
			print "\nINFO: None of the projects have changes for OTA\n"
	else:
		print "\nINFO: File '"+tmp_file+"' not found."
		print "#################################################"
		print " INFO: There seems to be no RPMs for OTA update. "
		print "#################################################"

	if (VERBOSE>=4): print "Returning from function "+sys_api._getframe().f_code.co_name+"()"
	return 0




# This function:
#	- is called by main() to process all repositories that have git-commits
#	- iterates over PROJECTS_WITH_CHANGES_FILE and understands which repositories need to be traversed
#	- iterates over the list of projects.
#	- Following are treated as special repositories (1) the_egg-platform (2) the_egg-tools (3) the_egg-third-party
#	- invokes special functions for special repositories and handle_source_repo() for normal repositories
#	- Each of the above functions identify the RPM that needs to be sent O-T-A and saves it in file - "tmp_mnfst_ptr"
def identify_rpms_for_ota():
	if (VERBOSE>=4): print "Entering function "+sys_api._getframe().f_code.co_name+"()"

	# Check if there are any projects with changes
	if os_api.access(PROJECTS_WITH_CHANGES_FILE, os_api.R_OK):
		try:
			proj_ptr = open(PROJECTS_WITH_CHANGES_FILE, 'r')
		except OSError as exc:
			print "ERROR! Something is terribly wrong with opening file for reading: "+PROJECTS_WITH_CHANGES_FILE
			print "Error: ",exc
			return -500
		if (VERBOSE>=1): print "Identifying RPMs for OTA package..."
		projects = list(proj_ptr)
		proj_ptr.close()
		projects = [x.strip() for x in projects] # Remove newline at end of each list entry
		projects = [x for x in projects if x] # Remove empty list entries
		max_count = len(projects)
		if (max_count > 0):
			if (VERBOSE>=2): print "\n********************************************************"
			if (VERBOSE>=2): print "INFO: Following projects have changes for OTA:",projects
			try:
				tmp_mnfst_ptr = open(TMP_MNFST_FILE, 'w')
			except OSError as exc:
				print "ERROR! Something is terribly wrong with opening file for writing: "+TMP_MNFST_FILE
				print "Error: ",exc
				return -500
			count=0
			for proj in projects:
				count=count+1
				if (VERBOSE>=1): print "["+str(count)+"/"+str(max_count)+"]",
				if (proj in SECIAL_REPOS):
					if (VERBOSE>=2): print "\tProject '"+proj+"' has changes. Need special handling..."
					if ("the_egg-platform" == proj):
						if (0 != handle_repo_platform(proj, tmp_mnfst_ptr)):
							tmp_mnfst_ptr.close()
							print "ERROR! handle_repo_platform() failed"
							return -1
					elif ("the_egg-tools" == proj):
						if (0 != handle_repo_tools(proj, tmp_mnfst_ptr)):
							tmp_mnfst_ptr.close()
							print "ERROR! handle_repo_tools() failed"
							return -1
					elif ("the_egg-third-party" == proj):
						if (0 != handle_repo_thirdparty(proj, tmp_mnfst_ptr)):
							tmp_mnfst_ptr.close()
							print "ERROR! handle_repo_thirdparty() failed"
							return -1
					###STITCH..BIN handling: Begin
					elif ("the_egg-preos" == proj):
						if (0 != handle_repo_preos(proj, tmp_mnfst_ptr)):
							tmp_mnfst_ptr.close()
							print "ERROR! handle_repo_preos() failed"
							return -1
					###STITCH..BIN handling: End
					else:
							print "ERROR! Project '"+proj+"' has changes. But don't know how to handle!!"
							print "ERROR! Implementation Pending!"
					''' code here '''
				else:
					#Save resulting RPM to TMP_MNFST_FILE
					if (VERBOSE>=2): print "\tSource repository '"+proj+"' has changes..."
					if (0 != handle_source_repo(proj, tmp_mnfst_ptr, EGG_GIT_REPO)):
						print "ERROR! handle_source_repo() failed: "+proj
						""" TODO: Begin comment """
						#TODO avoid this return to ignore any errors that occur in handling any of the source(normal) repositories
						tmp_mnfst_ptr.close()
						return -1
						""" TODO: End comment """

			if (VERBOSE>=1): print "INFO: Interim manifest saved to: "+TMP_MNFST_FILE
			tmp_mnfst_ptr.close()

		else:
			print "\nINFO: None of the projects have changes for OTA\n"
	else:
		print "\nINFO: File '"+PROJECTS_WITH_CHANGES_FILE+"' not found."
		print "INFO: There seems to be no changes for OTA updates"

	if (VERBOSE>=4): print "Returning from function "+sys_api._getframe().f_code.co_name+"()"
	return 0


# This function:
#	- runs "rpm -qp --provides FILE.rpm" command
#	- returns list in provides
def get_rpm_provides(rpm, provides):
	DIR = OUT_DIR+OTA_ZIP_FILENAME[:-4]+"/"
	CMD = "rpm -qp --provides "+DIR+rpm
	if (VERBOSE>=2): print "\tExecuting: " + CMD
	process=subp_api.Popen(CMD, stdout=subp_api.PIPE, stderr=subp_api.PIPE, shell=True)
	rpm_qr_output,rpm_qr_error = process.communicate()
	if (VERBOSE>=4): print "\tOutput:", rpm_qr_output,
	if (rpm_qr_error):
		print "\tCommand '",CMD,"' returned error!"
		print "\t"+rpm_qr_error
		return -1

	#"subprocess.popen returns a string, convert it into a list based on \n
	rpmqr_as_list = rpm_qr_output.split('\n')
	#Remove whitespaces & empty strings (usually the last list entry)
	rpmqr_as_list = [x.strip() for x in rpmqr_as_list] # Remove newline at end of each list entry
	rpmqr_as_list = [x for x in rpmqr_as_list if x] # Remove empty list entries & non-rpm file entries
	#Remove duplicate entries from rpmqr_as_list
	rpmqr_as_list = list(set(rpmqr_as_list))
	if (VERBOSE>=3): print "\tAfter removing duplicates(--provides):", rpmqr_as_list
	provides.extend(rpmqr_as_list)
	return 0



# This function:
#	- runs "rpm -qp --requires FILE.rpm" command
#	- returns list in requires
def get_rpm_requires(rpm, requires):
	DIR = OUT_DIR+OTA_ZIP_FILENAME[:-4]+"/"
	CMD = "rpm -qp --requires "+DIR+rpm
	if (VERBOSE>=2): print "\tExecuting: " + CMD
	process=subp_api.Popen(CMD, stdout=subp_api.PIPE, stderr=subp_api.PIPE, shell=True)
	rpm_qr_output,rpm_qr_error = process.communicate()
	if (VERBOSE>=4): print "\tOutput: " + rpm_qr_output,
	if (rpm_qr_error):
		print "\tCommand '",CMD,"' returned error!"
		print "\t"+rpm_qr_error
		return -1

	#"subprocess.popen returns a string, convert it into a list based on \n
	rpmqr_as_list = rpm_qr_output.split('\n')
	#Remove whitespaces & empty strings (usually the last list entry)
	rpmqr_as_list = [x.strip() for x in rpmqr_as_list] # Remove newline at end of each list entry
	rpmqr_as_list = [x for x in rpmqr_as_list if x] # Remove empty list entries & non-rpm file entries
	#Remove duplicate entries from rpmqr_as_list
	rpmqr_as_list = list(set(rpmqr_as_list))
	if (VERBOSE>=3): print "\tAfter removing duplicates(--requires):", rpmqr_as_list
	requires.extend(rpmqr_as_list)
	return 0


# This function:
#	- takes rpm_file_list as input.
#	- gives reordered_list as output.
#	Logic: Say an RPM R1 "provides" a module "m1". If RPM R2 "requires" module "m1",
#			then, R1 is listed on top and R2 is listed below R1
def reorder_manifest(rpm_file_list, reordered_list):
	if (VERBOSE>=4): print "Entering function "+sys_api._getframe().f_code.co_name+"()"

	provides_array={}
	requires_array={}

	###STITCH..BIN handling: Begin
	#If any filenames begin with "/", change them the contain only filename
	#This happens if stitch.normalos.bin or stitch.preos.bin are part of OTA
	rpm_file_list = [os_api.path.basename(rpm) for rpm in rpm_file_list]
	###STITCH..BIN handling: End

	rpm_count = len(rpm_file_list)
	if (VERBOSE>=3): print "Number of RPM lines =",rpm_count
	if (rpm_count == 0):
		print "WARNING: No RPMs to reorder."
	else:
		for rpm in rpm_file_list:
			if (VERBOSE>=3):
				print "\n----------------------------------------------"
				print "Processing --> "+rpm
			###STITCH..BIN handling: Begin
			if (not rpm.endswith(".rpm")): #If file is not an RPM
				if (VERBOSE>=2): print "Special file: "+rpm
				reordered_list.append(rpm)
				provides_array[rpm] = ""
				requires_array[rpm] = ""
				continue
			###STITCH..BIN handling: End

			provides=[]
			requires=[]
			if (0 != get_rpm_provides(rpm, provides)):
				print "ERROR: Failed to get RPM --provides list for:"+rpm
				return -400
			if (0 != get_rpm_requires(rpm, requires)):
				print "ERROR: Failed to get RPM --requires list for:"+rpm
				return -400
			provides_array[rpm] = provides
			requires_array[rpm] = requires

		'''
		provides_array[my.rpm] = ["a.so", "b.so", "pkgconfig(x)", "my"]
		requires_array[my.rpm] = ["p.so", "q.so", "pkgconfig(y)", "another"]
		'''
		if (VERBOSE>=2): print "========================================"
		counter=0
		while (counter < rpm_count):
			anything_changed = 0
			counter = counter+1
			for rpm in rpm_file_list:
				if (VERBOSE>=2): print "RPM="+rpm,
				for requires_single_item in requires_array[rpm]:
					for provide_list_oterator in rpm_file_list:
						if rpm != provide_list_oterator:
							if requires_single_item in provides_array[provide_list_oterator]:
								if (VERBOSE>=2): print "\tRequire=["+requires_single_item+"]",
								if (VERBOSE>=2): print "\tFound in=["+provide_list_oterator+"]"
								if provide_list_oterator not in reordered_list:
									anything_changed = 1
									reordered_list.append(provide_list_oterator)
								break # found what we needed. Skip the rest of loop
				if (VERBOSE>=2): print
				if rpm not in reordered_list:
					reordered_list.append(rpm)
			if (0==anything_changed):
				break;

		if (VERBOSE>=2):
			print "========================================"
			print "Old list("+str(len(rpm_file_list))+") = ",rpm_file_list
			print
			print "Reordered list("+str(len(reordered_list))+") = ",reordered_list
		if (len(rpm_file_list) != len(reordered_list)):
			print "ERROR: Number of RPM's after processing is not same as old list!"
			return -500


	if (VERBOSE>=4): print "Returning from function "+sys_api._getframe().f_code.co_name+"()"
	return 0


###############################Utility APIs##############################
def get_all_rpm_names_from_spec_files():
	spec_files=[]
	for root, dirs, files in os_api.walk(EGG_GIT_REPO):
		for file in files:
			if file.endswith(".spec"):
				f = os_api.path.join(root, file)
				spec_files.append(f)
				print "Found file: "+f

	for sfile in spec_files:
		rpm_list=[]
		if (0 != get_rpm_names(sfile, rpm_list)):
			print "ERROR: Failed to get RPM package name from spec file: "+sfile
			return -401
		else:
			if (len(rpm_list) > 0):
				#print sfile+":",
				for rpm in rpm_list:
					print "\t"+rpm,
				#print
			else:
				print "ERROR: '"+sfile+"' does not provide any RPMs!"
				return -502


#print "--Begin--"
#get_all_rpm_names_from_spec_files()
#print "--End--"
#return 0
###############################Utility APIs##############################


""" Entry function to the script """
def main():

	if (0 != initialize()): error_exit_initialize()
	if (0 != check_git_tag()): error_exit_checktag()
	if (0 != check_repo_for_changes()): error_exit_checkrepo4changes()
	if (0 != identify_rpms_for_ota()): error_exit_identifyrpm4ota()
	if (0 != prepare_ota_package(TMP_MNFST_FILE)): error_exit_prepareotapackage()

	print "Script completed."
	if (VERBOSE>=3): print "------------------(end)------------------"
	return 0



####-end-of-def main():
if __name__ =="__main__":
	main()
	my_exit(0, sys_api.argv[0]+" Success")


